﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Controls;

namespace AdwordsScraperGUI
{
    class TreeViewHelper
    {
        // one method do two different things?
        public static int getTreeviewSelectedIndex(TreeView tree_view, TreeViewItem selected_item = null)
        {
            int index = 0;
            TreeViewItem compare_item;
            if (selected_item == null)
            {
                compare_item = tree_view.SelectedItem as TreeViewItem;
            }
            else
            {
                compare_item = selected_item;
            }

            foreach (var item in tree_view.Items)
            {
                if (item == compare_item)
                {
                    return index;
                }
                index++;
            }

            // not found the selected item
            return 0;
        }

        public static int getKeywordListIndex(TreeViewItem parent_item, TreeViewItem list_item)
        {
            int index = 0;
            foreach (var item in parent_item.Items)
            {
                if (list_item == item)
                {
                    return index;
                }
                index++;
            }
            return 0;
        }

        public static string getTreeviewItemType(TreeViewItem item)
        {
            if (item.Items.Count > 0
                && getItemText(item) != "Link Profile" &&
                getItemText(item) != "Keyword Lists")
            {
                return "site";
            }
            else if (getItemText(item) == "Link Profile")
            {
                return "link_profile";
            }
            else if (getItemText(item) == "Keyword Lists")
            {
                return "keyword_lists";
            }
            // the parent website item won't meet this condition, it will be matched in the first condition
            else if (getItemText(item.Parent as TreeViewItem) == "Keyword Lists")
            {
                return "keyword_list";
            }

            return "none";
        }

        public static void expandTreeviewItems(TreeViewItem item)
        {
            item.IsExpanded = true;
            if (item.Items.Count > 0)
            {
                foreach (var child in item.Items)
                {
                    (child as TreeViewItem).IsExpanded = true;
                    expandTreeviewItems(child as TreeViewItem);
                }
            }
        }

        public static string getItemText(TreeViewItem item)
        {
            //return item.Header.ToString();
            return ((item.Header as StackPanel).Children[1] as Label).Content.ToString();
        }
    }
}
