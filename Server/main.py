import web
import json
import hashlib
import sys


license = {}
license['name'] = 'aaa'
license['type'] = 'trial'
license['email'] = 'aaa@aaa.com'
license['expiration'] = '2013-01-01'

license['key'] = hashlib.md5(license['name'] + license['email']).hexdigest()

licenses = {license['name']:license}


class getLicense:
	def GET(self, name):
		if name in licenses.keys():
			return json.dumps(licenses[name])
		# else:
		# 	return json.dumps({})
		
class key:
	def GET(self, name):
		m = hashlib.md5(name + licenses[name]['email'])
		return m.hexdigest()

if __name__ == '__main__':
	app = web.application(('/get-license/(.*)','getLicense','/key/(.*)','key'), globals())
	app.run()