﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdwordsScraperGUI.Core;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for AddKeywordListWindow.xaml
    /// </summary>
    public partial class AddKeywordListWindow : Window
    {
        Mediator mediator;
        public char delimiter;

        public AddKeywordListWindow()
        {
            InitializeComponent();
        }

        public AddKeywordListWindow(Mediator mediator)
        {
            InitializeComponent();
            this.mediator = mediator;
            this.comma_radioButton.IsChecked = true;
            this.Show();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {                        
            if (this.nameText.Text.Trim() == "")
            {
                MessageBox.Show("please enter the name");
                this.nameText.Focus();
                return;
            }

            // set delimiter
            if (this.comma_radioButton.IsChecked == true)
            {
                this.delimiter = ',';
            }
            else if (this.space_radioButton.IsChecked == true)
            {
                this.delimiter = ' ';
            }
            else if (this.newliine_radioButton.IsChecked == true)
            {
                this.delimiter = '\n';
            }

            try
            {
                this.mediator.finishAddKeywordList(this);
                this.Close();
            }
            catch (DuplicatedKeywordListException except)
            {
                MessageBox.Show("Duplicated keyword list existed.");
            }
        }
    }
}
