﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Reflection;

using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using AdwordsScraperGUI.Core;
using log4net;
using log4net.Config;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for IntergratedWindow.xaml
    /// </summary>
    public partial class IntergratedWindow : Window, IHasWebsitesList
    {
        public AnalyticsAPI analyticsAPI = null;
        public WebmasterToolAPI webmasterAPI = null;
        public List<Website> websites;
        public Mediator mediator;
        Action<DependencyProperty, double> updateProgressbarValue = null;

        private ContextMenu menu;        
        private ContextMenu keyword_list_menu;
        public AdwordsGrid adwords_grid = null;
        public System.Windows.Forms.NotifyIcon tray_icon;
        private System.Windows.Forms.ContextMenu tray_menu;
        //private TreeView 
        private int normal_width;

        private static readonly ILog log = LogManager.GetLogger(typeof(Object));

        public IntergratedWindow()
        {
            Window splash = new SplashWindow(0);

            
            //AppDomain.CurrentDomain.FirstChanceException += (s, e) =>
            //{
            //    var exception = (Exception)e.Exception;
            //    XmlConfigurator.Configure();
            //    log.Error(exception.Message);
            //    log.Error(exception.StackTrace);
            //};

            InitializeComponent();
            this.websites = Website.loadWebsites();
            this.updateProgressbarValue = (dp, val) => this.progress_bar.SetValue(dp, val);
            
            this.mediator = new Mediator(this);            

            splash.Close();
            
            
            if (!Core.Account.hasLicense())
            {
                new AccountSettingsWindow(this.mediator);
                this.Hide();
            }
            else
            {
                if (!License.isOutOfDate(License.license_url, Core.Account.load().username))
                {
                    this.buildUI();
                }
                else
                {
                    MessageBox.Show("License is out of date.");
                    new AccountSettingsWindow(this.mediator);
                    this.Hide();
                }
            }
            
            
            
            // checking of website and user credentials is in Window_Loaded()            
        }

        public void buildUI()
        {
            this.normal_width = Convert.ToInt32(this.Width);
            this.statusBar.Width = this.Width;
            this.progress_bar.Minimum = 0;
            this.progress_bar.Maximum = 100;
            
            this.buildSitesListOptionsMenu();
            this.buildKeywordListsOptionsMenu();
            this.buildWebsitesList();
            adwords_grid = new AdwordsGrid();
            adwords_grid.Margin = new Thickness(0, 0, 10, 0);
            adwords_grid.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            adwords_grid.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            Grid.SetColumn(adwords_grid, 3);
            Grid.SetRow(adwords_grid, 0);
            this.grid.Children.Add(adwords_grid);
            this.buildStatusbar();
            this.updateStatusBar();
            this.buildTrayIcon();            
        }

        private void buildStatusbar()
        {
            this.license_text.MouseEnter += delegate(Object sender, MouseEventArgs args)
            {
                this.Cursor = Cursors.Hand;
            };
            this.license_text.MouseLeave += delegate(object sender, MouseEventArgs args)
            {
                this.Cursor = Cursors.Arrow;
            };
            this.license_text.MouseLeftButtonUp += delegate(object sender, MouseButtonEventArgs args)
            {
                System.Diagnostics.Process.Start("IExplore.exe", Properties.Resources.site_url);
            };
            this.license_text.Text = "Licensed to " + Core.Account.load().username;
        }

        private void buildTrayIcon()
        {
            tray_icon = new System.Windows.Forms.NotifyIcon();
            tray_icon.Icon = new System.Drawing.Icon("data/google.ico");
            tray_icon.Visible = true;
            tray_icon.DoubleClick += delegate(object sender, EventArgs args)
            {
                this.Show();
                this.WindowState = WindowState.Normal;
            };
            buildTrayMenu();
            tray_icon.ContextMenu = tray_menu;
        }

        private void buildTrayMenu()
        {
            tray_menu = new System.Windows.Forms.ContextMenu();
            System.Windows.Forms.MenuItem help_item = new System.Windows.Forms.MenuItem();
            System.Windows.Forms.MenuItem version_item = new System.Windows.Forms.MenuItem();
            System.Windows.Forms.MenuItem close_item = new System.Windows.Forms.MenuItem();
            help_item.Text = "Help";
            help_item.Click += delegate(object sender, EventArgs args)
            {
                System.Diagnostics.Process.Start("IExplore.exe", Properties.Resources.site_url);
            };
            version_item.Text = "Version";
            version_item.Click += delegate(object sender, EventArgs args)
            {
                new VersionWindow(Properties.Resources.credits_text, Properties.Resources.credits_version);
            };
            close_item.Text = "Close";
            close_item.Click += delegate(object sender, EventArgs args)
            {
                System.Windows.Application.Current.Shutdown();
            };

            tray_menu.MenuItems.Add(help_item);
            tray_menu.MenuItems.Add(version_item);
            tray_menu.MenuItems.Add(close_item);
        }
        
        private void updateStatusBar(){
            this.statusBar.Width = this.Width;
            this.progress_bar.Width = this.statusBar.Width / 5;
            this.loading_text.Width = this.statusBar.Width / 5 * 2;
            this.license_text.Width = this.statusBar.Width / 5 * 2;
        }

        public void updateProgressbar(double value)
        {
            this.Dispatcher.Invoke(updateProgressbarValue,
                System.Windows.Threading.DispatcherPriority.Background,
                ProgressBar.ValueProperty,
                value
            );
        }

        public List<Website> getWebsitesList()
        {
            return this.websites;
        }

        public void initApiIfNotExist()
        {            
            if (this.analyticsAPI == null)
                this.analyticsAPI = new AnalyticsAPI();
            if (this.webmasterAPI == null)
                this.webmasterAPI = new WebmasterToolAPI();
        }        

        public void refreshApi()
        {
            this.analyticsAPI = new AnalyticsAPI();
            this.webmasterAPI = new WebmasterToolAPI();
        }

        // build the websites ListView
        public void buildWebsitesList()
        {
            for (int i=websitesList.Items.Count-1; i >= 0; i--){
                websitesList.Items.RemoveAt(i);
            }
            

            //this.websitesList.DataContext = this.websites;
            foreach (Website site in this.websites)
            {
                TreeViewItem site_item = new TreeViewItem();
                //site_item.Header = site.Name;
                site_item.Header = new ImageLabel(site.Name, "data/world.png");
                this.websitesList.Items.Add(site_item);

                TreeViewItem keyword_lists_item = new TreeViewItem();
                //keyword_lists_item.Header = "Keyword Lists";
                keyword_lists_item.Header = new ImageLabel("Keyword Lists", "data/folder_horizontal.png");
                keyword_lists_item.Expanded += delegate(Object sender, RoutedEventArgs args)
                {
                    keyword_lists_item.Header = new ImageLabel("Keyword Lists", "data/folder_horizontal_open.png");
                };
                keyword_lists_item.Collapsed += delegate(Object sender, RoutedEventArgs args)
                {
                    keyword_lists_item.Header = new ImageLabel("Keyword Lists", "data/folder_horizontal.png");
                };
                site_item.Items.Add(keyword_lists_item);

                // website is not added manually
                if (site.keyword_lists != null)
                {
                    foreach (KeywordList list in site.keyword_lists)
                    {
                        TreeViewItem list_item = new TreeViewItem();
                        //list_item.Header = list.name;
                        list_item.Header = new ImageLabel(list.name, "data/doc_table.png");
                        keyword_lists_item.Items.Add(list_item);
                    }
                }

                TreeViewItem linkprofile_item = new TreeViewItem();
                linkprofile_item.Header = new ImageLabel("Link Profile", "data/link.png");
                site_item.Items.Add(linkprofile_item);
            }
        }

        

        private void buildKeywordListsOptionsMenu()
        {
            this.keyword_list_menu = new ContextMenu();
            MenuItem new_keyword_list_item = new MenuItem();
            new_keyword_list_item.Header = "Add new list";
            new_keyword_list_item.Click += delegate
            {
                new AddKeywordListWindow(this.mediator);
            };

            this.keyword_list_menu.Items.Add(new_keyword_list_item);
        }

        // used for both Website Item and KeywordList Item
        private void buildSitesListOptionsMenu()
        {
            this.menu = new ContextMenu();
            MenuItem rename_item = new MenuItem();
            rename_item.Header = "Rename";
            MenuItem delete_item = new MenuItem();
            delete_item.Header = "Delete";

            delete_item.Click += delegate
            {
                int site_index = 0;
                TreeViewItem selected_item = this.websitesList.SelectedItem as TreeViewItem;              

                if (TreeViewHelper.getTreeviewItemType(selected_item) == "site")
                {
                    site_index = TreeViewHelper.getTreeviewSelectedIndex(this.websitesList);
                    this.websites.RemoveAt(site_index);
                }
                else if (TreeViewHelper.getTreeviewItemType(selected_item) == "keyword_list")
                {
                    site_index = TreeViewHelper.getTreeviewSelectedIndex(this.websitesList,
                        ((selected_item.Parent as TreeViewItem).Parent as TreeViewItem));
                    int keyword_list_index = TreeViewHelper.getKeywordListIndex(selected_item.Parent as TreeViewItem, selected_item);
                    this.websites[site_index].keyword_lists.RemoveAt(keyword_list_index);
                }
                
                this.buildWebsitesList();
                Website.saveWebsites(this.websites);

                // restore Treeview
                if (TreeViewHelper.getTreeviewItemType(selected_item) == "keyword_list")
                {
                    TreeViewHelper.expandTreeviewItems(this.websitesList.Items[site_index] as TreeViewItem);
                }
            };
            rename_item.Click += delegate
            {
                RenameDialog rename_dialog = new RenameDialog(this);
                rename_dialog.Show();
            };

            this.menu.Items.Add(rename_item);
            this.menu.Items.Add(delete_item);
        }    

        private void websiteItem_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Credential.isEmptyCredential())
            {
                MessageBox.Show("some credential field is empty");
                return;
            }

            LoadingWindow loading = new LoadingWindow(this);
            loading.Topmost = false;
            
            this.initApiIfNotExist();
            TreeViewItem item = (TreeViewItem)sender;
            // If DoubleClick is triggered on child item, the event will also trigger on the parent item
            if(item == websitesList.SelectedItem){
                if (TreeViewHelper.getTreeviewItemType(item) == "site")
                {
                    this.mediator.clickWebsiteItem(this.websites[TreeViewHelper.getTreeviewSelectedIndex(websitesList)]);
                }
                else if (TreeViewHelper.getTreeviewItemType(item) == "keyword_list")
                {
                    this.mediator.clickKeywordListItem();
                }
                else if (TreeViewHelper.getTreeviewItemType(item) == "link_profile")
                {
                    this.mediator.clickLinkProfileItem();
                    
                }
            }

            loading.Close();
        }

        private void websiteItem_RightButton(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;
            if (item.ContextMenu == null && item == websitesList.SelectedItem)
            {
                switch (TreeViewHelper.getTreeviewItemType(item))
                {
                    case "site":
                        this.menu.PlacementTarget = item;
                        this.menu.IsOpen = true;
                        break;
                    case "keyword_lists":
                        this.keyword_list_menu.PlacementTarget = item;
                        this.keyword_list_menu.IsOpen = true;
                        break;
                    case "keyword_list":
                        this.menu.PlacementTarget = item;
                        this.menu.IsOpen = true;
                        break;
                    default:
                        break;
                }
                
            }
        }
		
		void addWebsiteButton_Click(object sender, RoutedEventArgs e)
		{
            this.mediator.createAddWebsiteWindow(this);
		}

        private void setCredentialsButton_Click(object sender, RoutedEventArgs e)
        {
            CredentialSettings credential_settings = new CredentialSettings(this.mediator);
        }

        private void addManualWebsiteButton_Click(object sender, RoutedEventArgs e)
        {
            this.mediator.createAddManualSiteWindow(this);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // add website if no website is in list.
            if (this.websites.Count == 0)
            {
                this.mediator.createAddSiteTypesWindow(this);
            }

            Credential credential = Credential.load();
            if (!credential.first_use)
            {
                new CredentialSettings();
                credential.first_use = true;
                credential.save();
            }
        }

        private void gridSplitter2_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            if (this.contentTabs.Width + e.HorizontalChange > 0)
            {
                this.contentTabs.Width += e.HorizontalChange;
            }
            if (this.adwords_grid.adwordsGrid.Width - e.HorizontalChange > 0 && this.adwords_grid.ideasList.Width - e.HorizontalChange > 0)
            {
                this.adwords_grid.Width -= e.HorizontalChange;
                this.adwords_grid.adwordsGrid.Width -= e.HorizontalChange;
                this.adwords_grid.ideasList.Width -= e.HorizontalChange;

                // the column at the right side of wordnet grid
                double last_column_width = ((this.adwords_grid.adwordsTabs.Items[1] as TabItem).Content as WordnetGrid).wordnet_grid.ColumnDefinitions[3].Width.Value;
                if (((this.adwords_grid.adwordsTabs.Items[1] as TabItem).Content as WordnetGrid).wordnet_grid.Width - e.HorizontalChange > 0 && ((this.adwords_grid.adwordsTabs.Items[1] as TabItem).Content as WordnetGrid).resultText.Width - e.HorizontalChange > 0 && last_column_width - e.HorizontalChange >= 0)
                {
                    ((this.adwords_grid.adwordsTabs.Items[1] as TabItem).Content as WordnetGrid).wordnet_grid.Width -= e.HorizontalChange;
                    ((this.adwords_grid.adwordsTabs.Items[1] as TabItem).Content as WordnetGrid).resultText.Width -= e.HorizontalChange;
                    ((this.adwords_grid.adwordsTabs.Items[1] as TabItem).Content as WordnetGrid).wordnet_grid.ColumnDefinitions[3].Width = new GridLength(last_column_width - e.HorizontalChange);
                }
                
                
                
                
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.grid.Width = this.Width;
            double tentative_contentTabs_width = this.grid.Width - this.websitesList.Width - 570;
            if (tentative_contentTabs_width < 1)
            	tentative_contentTabs_width = 1;
            this.contentTabs.Width = tentative_contentTabs_width;
            this.grid.ColumnDefinitions[1].Width = new GridLength(this.contentTabs.Width+10, GridUnitType.Pixel);
            this.updateStatusBar();
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Maximized)
            {
                this.Width = System.Windows.SystemParameters.PrimaryScreenWidth;
            }
            else if (this.WindowState == WindowState.Normal)
            {
                this.Width = this.normal_width;
            }
            else if (this.WindowState == WindowState.Minimized)
            {
                this.Hide();
            }
        }

        private void sitetypesButton_Click(object sender, RoutedEventArgs e)
        {
            new SiteTypesWindow();
        }
				
		void window1_Closed(object sender, EventArgs e)
		{
			this.tray_icon.Visible = false;
			this.tray_icon.Dispose();
		}

    }

    class ClosableTabItem : TabItem
    {
        public ClosableTabItem(string name)
        {
            StackPanel panel = new StackPanel();
            Label text_label = new Label();
            Label close = this.buildCloseLabel();

            text_label.Content = name;

            panel.Orientation = System.Windows.Controls.Orientation.Horizontal;
            panel.Children.Add(text_label);
            panel.Children.Add(close);

            this.Header = panel;
        }

        public ClosableTabItem()
        {
        }

        public Label buildCloseLabel()
        {
            Label close = new Label();
            close.Content = "X";
            close.FontSize = 10;
            close.MouseEnter += delegate
            {
                SolidColorBrush brush = new SolidColorBrush(Colors.Gray);
                close.Background = brush;
            };
            close.MouseLeave += delegate
            {
                SolidColorBrush brush = new SolidColorBrush(Colors.White);
                close.Background = brush;
            };

            // remove tab item
            close.MouseUp += delegate
            {
                TabControl tabs = this.Parent as TabControl;
                tabs.Items.Remove(this);
            };

            return close;
        }
    }

    class ImageLabel : StackPanel
    {
        public ImageLabel(string name, string img_path)
        {
            string assembly_name = Assembly.GetExecutingAssembly().GetName().Name;
            this.Orientation = Orientation.Horizontal;

            Label label = new Label();
            label.Content = name;
            label.Padding = new Thickness(2);
            Image img = new Image();
            var uri_source = new Uri(@""+ assembly_name +";component/" + img_path, UriKind.Relative);
            img.Source = new BitmapImage(uri_source);

            this.Children.Add(img);
            this.Children.Add(label);
        }
    }
}
