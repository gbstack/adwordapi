﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Text.RegularExpressions;

namespace AdwordsScraperGUI.Core
{
    public class WebmasterScraper
    {
        System.Windows.Forms.WebBrowser browser;
        System.Windows.Controls.TreeViewItem selected_item;
        LoadingWindow loading_window = null;
        public delegate void delegateNavigate();
        Mediator mediator;
        Website website;
        LinkProfile link_profile;
        Logger logger;

        int login_times = 0;
        int home_times = 0;
        

        public WebmasterScraper(Mediator mediator, Website website, LinkProfile link_profile)
        {
            this.browser = new System.Windows.Forms.WebBrowser();   
            this.website = website;
            this.link_profile = link_profile;
            this.mediator = mediator;
            this.logger = new Logger("scraper_log.txt");
        }

        public void documentComplete(Object sender, WebBrowserDocumentCompletedEventArgs args)
        {
            Credential credential = Credential.load();
            logger.write(browser.Url.AbsoluteUri + "\n\n");
            logger.write(browser.DocumentText + "\n\n\n\n");
            // arrived "accounts.google.com/ServiceLogin" login page
            if (browser.Url.AbsoluteUri.Contains("ServiceLogin"))
            {
                HtmlElement head = browser.Document.GetElementsByTagName("head")[0];
                HtmlElement script_ele = browser.Document.CreateElement("script");
                script_ele.SetAttribute("text", "document.getElementById('Email').value = '" + credential.username + "'; document.getElementById('Passwd').value='" + credential.password + "'; document.getElementById('signIn').click();");
                head.AppendChild(script_ele);
                this.mediator.updateProgressbar(60);
                login_times++;
            }
            if (login_times > 3)
            {
                this.loading_window.Close();
                browser.DocumentCompleted -= documentComplete;
                MessageBox.Show("username or password invalid");                
                logger.close();
            }

            // arrived google.com/settings
            if (browser.Url.AbsoluteUri.Contains("settings"))
            {
                browser.Navigate("http://www.google.com/webmasters/tools");
                this.mediator.updateProgressbar(70);
            }
            // arrived webmasters tools homepage
            if (browser.Url.AbsoluteUri.Contains("home"))
            {
                browser.Navigate("http://www.google.com/webmasters/tools/external-links-domain?siteUrl=" + HttpUtility.UrlEncode(website.URL) + "&firsttier.s=500");
                this.mediator.updateProgressbar(80);
                home_times++;
            }
            if (home_times > 2)
            {
                this.loading_window.Close();
                browser.DocumentCompleted -= documentComplete;
                MessageBox.Show("website not verified");
                logger.close();
            }

            // arrived external links page
            if (browser.Url.AbsoluteUri.Contains("external-links-domain"))
            {
                Regex re = new Regex(@"<td\sclass=\'leftmost\surl\'><a[^>]*?>(.*?)<\/a><\/td>\s<td[^>]*?>(.*?)<\/td>");
                MatchCollection matches = re.Matches(browser.DocumentText);
                if (matches.Count > 0)
                {
                    foreach (Match match in matches)
                    {
                        link_profile.sites.Add(match.Groups[1].Value, Convert.ToInt32(match.Groups[2].Value));
                    }
                }
                this.mediator.updateProgressbar(90);

                // new Link Profile Tab
                if (!mediator.hasDuplicatedTab(TreeViewHelper.getItemText(selected_item), mediator.parent_window.contentTabs))
                {
                    ClosableTabItem tab = new ClosableTabItem(website.Name + " Link Profile");
                    tab.Content = new LinkProfileTemplate(link_profile);
                    tab.IsSelected = true;
                    mediator.parent_window.contentTabs.Items.Add(tab);
                }
                this.mediator.updateProgressbar(100);
                this.mediator.parent_window.loading_text.Text = "Done";
                logger.close();
                loading_window.Close();
            }
        }

        public void bindEvent(System.Windows.Controls.TreeViewItem selected_item)
        {
            
            this.selected_item = selected_item;
            browser.DocumentCompleted += documentComplete;
            
        }

        public void start()
        {
            if (this.browser.InvokeRequired)
            {
                this.browser.Invoke(new delegateNavigate(start));
            }
            else
            {
                this.browser.Navigate("http://accounts.google.com/ServiceLogin");
                this.loading_window = new LoadingWindow(mediator.parent_window);
            }
        }
    }
}
