﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;

namespace AdwordsScraperGUI.Core
{
    public class FilePersister
    {        
        public static FilePersister load<T>(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);
            string text = sr.ReadToEnd();
            sr.Close();
            fs.Close();

            if (text != "")
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                // decryption
                FilePersister persister = (FilePersister)serializer.Deserialize(text, typeof(T));
                return persister;
            }
            else
            {
                return null;
            }
        }

        public void save(string filename)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            StreamWriter sw = new StreamWriter(filename);
            // encryption            
            sw.Write(serializer.Serialize(this));
            sw.Close();
        }
    }
}
