﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;

namespace AdwordsScraperGUI.Core
{
    public class Account : FilePersister
    {
        public string username = null;
        public string email = null;
        public License license = null;
        public static new string filename = "accounts";

        public Account()
        {                        
        }

        public static bool hasLicense()
        {
            Account account = Account.load();
            if (account.license != null )
            {
                if (account.license.key != null)
                {
                    if (account.license.key.Length == 0)
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public static Account load()
        {
            return FilePersister.load<Account>(filename) == null ? new Account() : (Account)FilePersister.load<Account>(filename);
        }

        public void save()
        {
            (this as FilePersister).save(filename);
        }
    }
}
