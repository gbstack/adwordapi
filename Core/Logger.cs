﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace AdwordsScraperGUI.Core
{
    public class Logger
    {
        string filename;
        FileStream file_stream;
        StreamWriter writer;

        public Logger(string filename)
        {
            //file_stream = new FileStream(filename, FileMode.Append);
            file_stream = new FileStream(filename, FileMode.OpenOrCreate);
            writer = new StreamWriter(file_stream);
        }

        public void write(string text)
        {
            
            writer.Write(text);
            
        }

        public void close()
        {
            writer.Close();
            file_stream.Close();
        }
    }
}
