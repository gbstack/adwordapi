﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdwordsScraperGUI.Core
{
    public class SearchDetail
    {
        public string keyword { get; set; }
        public int organic_visits { get; set; }
        public int paid_visits { get; set; }

        public SearchDetail()
        {
            this.keyword = "";
            this.organic_visits = 0;
            this.paid_visits = 0;
        }
    }
}
