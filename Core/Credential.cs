﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using System.Windows;

namespace AdwordsScraperGUI.Core
{
    class Credential
    {
        public string client_key = "";
        public string client_secret = "";
        public string analytics_username = "";
        public string analytics_password = "";
        public string username = "";
        public string password = "";
        public bool first_use = false;

        public string encrypt(string text)
        {
            byte[] bytes = (new UnicodeEncoding()).GetBytes(text);
            return Convert.ToBase64String(bytes);
        }

        public string decrypt(string text)
        {
            return (new UnicodeEncoding()).GetString(Convert.FromBase64String(text));
        }

        public static Credential load(){
            FileStream fs = new FileStream("credentials", FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);
            string text = sr.ReadToEnd();
            sr.Close();
            fs.Close();

            if (text != "")
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                // decryption
                Credential credential = (Credential)serializer.Deserialize(text, typeof(Credential));
                credential.password = credential.decrypt(credential.password);
                credential.analytics_password = credential.decrypt(credential.analytics_password);
                return credential;
            }
            else
            {
                return new Credential();
            }
        }

        public void save()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            StreamWriter sw = new StreamWriter("credentials");
            // encryption
            this.password = this.encrypt(this.password);
            this.analytics_password = this.encrypt(this.analytics_password);
            sw.Write(serializer.Serialize(this));
            sw.Close();
        }

        public static bool isEmptyCredential()
        {
            Credential credential = Credential.load();
            if (credential.username.Trim() == "" || credential.password.Trim() == "" || credential.client_key.Trim() == "" || credential.client_secret == "" || credential.analytics_username == "" || credential.analytics_password == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
