﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using NHibernate;
using NHibernate.Cfg;

namespace AdwordsScraperGUI.Core
{
    class Persister
    {
        ISessionFactory session_factory;
        ISession session;
        ITransaction transaction;

        public Persister()
        {
            Configuration config = new Configuration();
            config.AddAssembly(Assembly.GetCallingAssembly());
            this.session_factory = config.BuildSessionFactory();
            this.session = this.session_factory.OpenSession();
            this.transaction = this.session.BeginTransaction();
        }

        public void save(Object obj)
        {
            this.session.Save(obj);
            this.transaction.Commit();
        }
    }
}
