﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

using DotNetOpenAuth.OAuth2;
using Google.Apis.Authentication;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using Google.Apis.Util;
using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;

using Google.Apis.Samples.Helper;


namespace AdwordsScraperGUI.Core
{
    // Authentication with OAuth 2
    public class AnalyticsAPI
    {
        private string client_id;
        private string client_secret;
        public AnalyticsService service;

        public static string STORAGE = "storage_analytics";
        public static string STORAGE_KEY = "key_analytics";

        // read client id and client secret from file.
        public AnalyticsAPI()
        {
            Credential credential = Credential.load();            
            this.client_id = credential.client_key;
            this.client_secret = credential.client_secret;
            this.service = new AnalyticsService(this.createAuthenticator());
        }

        public AnalyticsAPI(string client_id, string client_secret)
        {
            this.client_id = client_id;
            this.client_secret = client_secret;
            this.service = new AnalyticsService(this.createAuthenticator());
        }

        // need check whether the access token(state) is valid
        public List<Website> getWebsites()
        {
            List<Website> websites = new List<Website>();
            try
            {
                foreach (Google.Apis.Analytics.v3.Data.Account account in this.service.Management.Accounts.List().Fetch().Items)
                {
                    foreach (Webproperty property in this.service.Management.Webproperties.List(account.Id).Fetch().Items)
                    {
                        websites.Add(new Website().getFromWebProperty(property));
                    }
                }
            }
            catch (Exception e)
            {
                // maybe caused by invalid state => invalid credentials
                if (e is Google.GoogleApiRequestException)
                {
                    MessageBox.Show("Please check whether your credentials is valid.");
                    return websites;
                }
                else
                {
                    MessageBox.Show(e.Message);
                }
                
            }

            return websites;
        }

        public WebsiteDetail getWebsiteDetails(Website website, AnalyticsReportParameter param)
        {
            string profile_id = "";
            // isn't the reporting data same in different profiles of same webproperty?
            foreach(Profile profile in 
                this.service.Management.Profiles.List(website.web_property.AccountId, website.web_property.Id).Fetch().Items
            ){
                profile_id = profile.Id;             
            }

            
            // get reporting data
            WebsiteDetail detail = new WebsiteDetail();
            GaReport report = this.service.Report.Get(param.end_date, param.start_date, param.getMetricsString(), "ga:" + profile_id).Fetch();

            // check whether the report's data is null
            if (report.Rows != null)
            {
                foreach (List<string> row_values in report.Rows)
                {
                    foreach (string metric in param.metrics)
                    {
                        detail.attributes[metric] = row_values[param.metrics.IndexOf(metric)];
                    }
                }
                detail.calculatePagesPerVisit();
            }
            else
            {
                detail.error_info = "No Analytics Data";
            }

            return detail;
        }

        

        private IAuthenticator createAuthenticator()
        {
            var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description);
            provider.ClientIdentifier = this.client_id;
            provider.ClientSecret = this.client_secret;
            return new OAuth2Authenticator<NativeApplicationClient>(provider, getAnalyticsAuthorizationState);
        }        

        private IAuthorizationState getAnalyticsAuthorizationState(NativeApplicationClient client)
        {            
            // use existing access token
            IAuthorizationState state = AuthorizationMgr.GetCachedRefreshToken(STORAGE, STORAGE_KEY);
            if (state != null)
            {
                try
                {
                    // when access token is invalid, refresh the token
                    client.RefreshToken(state);
                    return state;
                }
                // this ProtocolException may be caused by invalid credentials
                catch (DotNetOpenAuth.Messaging.ProtocolException e)
                {
                    // should be replaced by logging
                    return state;
                }
            }

            // create new access token if there is not exsiting access token
            try
            {
                state = AuthorizationMgr.RequestNativeAuthorization(client,Properties.Resources.server_response, Properties.Resources.redirect_url, AnalyticsService.Scopes.AnalyticsReadonly.GetStringValue());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            if (state != null)
            {
                AuthorizationMgr.SetCachedRefreshToken(STORAGE, STORAGE_KEY, state);
            }
            return state;
        }

        public static void getNewToken(string client_key, string client_secret)
        {            
            var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description);
            provider.ClientIdentifier = client_key;
            provider.ClientSecret = client_secret;

            IAuthorizationState state = null;
            try
            {
                state = AuthorizationMgr.RequestNativeAuthorization(provider, Properties.Resources.server_response, Properties.Resources.redirect_url, AnalyticsService.Scopes.AnalyticsReadonly.GetStringValue());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            if (state != null)
            {
                AuthorizationMgr.SetCachedRefreshToken(STORAGE, STORAGE_KEY, state);
            }
        }
    }

    public class AnalyticsReportParameter
    {
        public string start_date;
        public string end_date;
        public List<string> metrics;

        public AnalyticsReportParameter(string start_date, string end_date)
        {
            this.start_date = start_date;
            this.end_date = end_date;
            metrics = 
                new List<string> { 
                    "ga:visits", "ga:pageviews", "ga:newVisits", "ga:avgTimeOnSite", "ga:visitBounceRate", "ga:visitors"
                };
        }

        // used in Reporting API
        public string getMetricsString()
        {
            return String.Join(",", this.metrics);
        }
    }

    
}
