﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Windows;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using System.Diagnostics;

namespace AdwordsScraperGUI.Core
{
    public class License
    {
        public string name;
        public string key;
        public string type;
        public string expiration;
        public static string license_url = "http://localhost:8080/get-license";

        public License()
        {

        }

        public static License get(string url, string name)
        {
            License license = null;
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            url += "/" + name;
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception e)
            {
                MessageBox.Show("HTTP Connection failed.");
                // if we cannot get or verify license, app shouldnt start                
                Process.GetCurrentProcess().Kill();
                return new License();
            }
            StreamReader reader = new StreamReader(response.GetResponseStream());
            try
            {
                license = (License)serializer.Deserialize(reader.ReadToEnd(), typeof(License));
            }
            catch (ArgumentException e)
            {
                return license;
            }
            return license;
        }        

        public static string generateKey(string name, string email)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            string result_text = "";
            byte[] result = MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(name + email));
            foreach (byte b in result)
            {
                string hex_byte = b.ToString("x");
                result_text += (hex_byte.Length == 1 ? "0" : "") + hex_byte;
            }
            return result_text;
        }

        public static bool isOutOfDate(string url, string name)
        {
            License license = License.get(url, name);
            if (license == null || license.expiration == null)
            {
                MessageBox.Show("License null or License expiration is null");
                return true;
            }
            else
            {
                // set default expiration to yesterday (cannot be null)
                DateTime expiration = DateTime.Today.AddDays(-1);
                DateTime today = DateTime.Today;
                try
                {
                    expiration = DateTime.Parse(license.expiration);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Invalid date format");                   
                    return true;
                }

                // not reached expiration
                if (DateTime.Compare(expiration, today) > 0)
                {
                    return false;
                }
                // reached expiration
                else
                {
                    return true;
                }
            }
        }
    }
}
