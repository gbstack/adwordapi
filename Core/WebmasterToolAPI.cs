﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

using Google.GData.Client;
using Google.GData.WebmasterTools;

namespace AdwordsScraperGUI.Core
{
    // Authentication with ClientLogin
    public class WebmasterToolAPI
    {
        private WebmasterToolsService service;

        // read username and password from file
        public WebmasterToolAPI()
        {
            Credential credential = Credential.load();

            this.service = new WebmasterToolsService("Webmaster");
            this.service.setUserCredentials(credential.username, credential.password);
        }

        public WebmasterToolAPI(string username, string password)
        {
            this.service = new WebmasterToolsService("Webmaster");
            this.service.setUserCredentials(username, password);
        }

        public List<Website> getWebsites()
        {
            List<Website> websites = new List<Website>();
            SitesFeed feed = null;
            try
            {
                feed = this.service.Query(new SitesQuery());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return websites;
            }

            try
            {
                foreach (SitesEntry entry in feed.Entries)
                {
                    websites.Add(new Website().getFromSitesEntry(entry));
                }
            }
            catch (NullReferenceException e)
            {
                MessageBox.Show("No webmaster websites.");
            }
            
            return websites;
        }

        public WebsiteDetail getWebsiteDetails(Website website)
        {
            WebsiteDetail detail = new WebsiteDetail();
            CrawlIssuesQuery query = 
                new CrawlIssuesQuery(
                    CrawlIssuesQuery.CreateCustomUri(
                        HttpUtility.UrlEncode(Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(website.URL)))
                    )
                );
            CrawlIssuesFeed feed = this.service.Query(query);

            foreach (CrawlIssuesEntry entry in feed.Entries)
            {
                if (detail.attributes.ContainsKey(entry.IssueType))
                {
                    detail.attributes[entry.IssueType] = (Convert.ToInt32(detail.attributes[entry.IssueType]) + 1).ToString();
                }
                else
                {
                    detail.attributes[entry.IssueType] = "1";
                }
            }

            return detail;
        }

        public void getLinkProfile()
        {

        }
    }
}
