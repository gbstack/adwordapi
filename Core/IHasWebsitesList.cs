﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdwordsScraperGUI.Core
{
    public interface IHasWebsitesList
    {
        List<Website> getWebsitesList();

        void buildWebsitesList();

    }
}
