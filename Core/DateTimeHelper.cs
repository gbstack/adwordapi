﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdwordsScraperGUI.Core
{
    class DateTimeHelper
    {
        public static string getFormattedOutput(DateTime date)
        {
            string month = date.Month.ToString().Length == 1 ? "0" + date.Month.ToString() : date.Month.ToString();
            string day = date.Day.ToString().Length == 1 ? "0" + date.Day.ToString() : date.Day.ToString();
            return date.Year.ToString() + "-" + month + "-" + day;
        }
    }
}
