﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdwordsScraperGUI.Core
{
    class SiteTypes : FilePersister
    {
        public Dictionary<string, string> types_rules = null;
        public static string filename = "site_types";
        public static string sitetypes_url = "http://localhost:8080/get-sitetypes";

        public static SiteTypes load()
        {
            SiteTypes site_types = FilePersister.load<SiteTypes>(filename) == null ? new SiteTypes() : (SiteTypes)FilePersister.load<SiteTypes>(filename);
            if (site_types.types_rules == null)
            {
                site_types.types_rules = new Dictionary<string, string>();
            }
            return site_types;
        }

        public void save()
        {
            (this as FilePersister).save(filename);
        }
    }
}
