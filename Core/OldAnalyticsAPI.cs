﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

using Google.GData.Client;
using Google.GData.Analytics;
using log4net;
using log4net.Config;

using AdwordsScraperGUI.Core;

namespace AdwordsScraperGUI.Core
{
    class OldAnalyticsAPI
    {
        AnalyticsService service = null;
        string query_uri = "https://www.googleapis.com/analytics/v2.4/data";
        private DataFeed feed = null;
        private static readonly ILog log = LogManager.GetLogger(typeof(OldAnalyticsAPI));

        public OldAnalyticsAPI()
        {            
            XmlConfigurator.Configure();
            Credential credential = Credential.load();
            this.service = new AnalyticsService("get_keywords_search");
            this.service.setUserCredentials(credential.analytics_username, credential.analytics_password);
            //log.Info("init finished. Analytics username:" + credential.analytics_username);
        }

        public List<SearchDetail> getSearchDetails(Website website, AnalyticsAPI new_API, string start_date, string end_date)
        {            
            List<SearchDetail> search_details = new List<SearchDetail>();
            Dictionary<string, SearchDetail> keyword_detail_map = new Dictionary<string, SearchDetail>();

            if (website.web_property == null)
            {
                MessageBox.Show("invalid Analytics credentials or empty Analytics data");                
                return search_details;
            }

            string profile_id = "";
            try
            {
                foreach (Google.Apis.Analytics.v3.Data.Profile profile in
                    new_API.service.Management.Profiles.List(website.web_property.AccountId, website.web_property.Id).Fetch().Items)
                {
                    profile_id = profile.Id;
                }
            }
            catch (Google.GoogleApiRequestException)
            {
                return search_details;
            }

            // Execute Query
            DataQuery query = new DataQuery(query_uri);
            query.Ids = "ga:" + profile_id;
            query.Metrics = "ga:visits";
            query.Dimensions = "ga:keyword, ga:medium";
            query.GAStartDate = start_date;
            query.GAEndDate = end_date;

            try
            {
                feed = this.service.Query(query);
            }
            catch (InvalidCredentialsException e)
            {
                MessageBox.Show("Invalid Analytics username or password");
                //log.Error(e.Message);
                //log.Error(e.StackTrace);
                return search_details;
            }

            //build Search Details
            foreach (var atom_entry in feed.Entries)
            {
                string current_keyword = "";
                string current_medium = "";
                DataEntry entry = atom_entry as DataEntry;
                foreach (Dimension dimension in entry.Dimensions)
                {                    
                    if (dimension.Name == "ga:keyword")
                    {
                        current_keyword = dimension.Value;
                        if (!keyword_detail_map.ContainsKey(current_keyword))
                        {
                            keyword_detail_map.Add(current_keyword, new SearchDetail());
                            keyword_detail_map[current_keyword].keyword = current_keyword;
                        }
                    }
                    else if (dimension.Name == "ga:medium")
                    {
                        current_medium = dimension.Value;
                    }
                }

                foreach (Metric metric in entry.Metrics)
                {
                    if (metric.Name == "ga:visits")
                    {
                        if (current_medium == "organic")
                        {
                            keyword_detail_map[current_keyword].organic_visits = Convert.ToInt32(metric.Value);
                        }
                        else if (current_medium == "ppc")
                        {
                            keyword_detail_map[current_keyword].paid_visits = Convert.ToInt32(metric.Value);
                        }
                    }
                }
            }            
            return keyword_detail_map.Values.ToList();
        }

        public LinkProfile getLinkProfile(Website website, AnalyticsAPI new_API)
        {
            LinkProfile link_profile = new LinkProfile();

            // check if Webproperty is null
            if (website.web_property == null)
            {
                MessageBox.Show("invalid Analytics credentials or empty analytics data");
                return link_profile;
            }

            string profile_id = "";
            foreach (Google.Apis.Analytics.v3.Data.Profile profile in
                new_API.service.Management.Profiles.List(website.web_property.AccountId, website.web_property.Id).Fetch().Items)
            {
                profile_id = profile.Id;
            }

            // Execute Query
            DataQuery query = new DataQuery(query_uri);
            query.Ids = "ga:" + profile_id;
            query.Metrics = "ga:visits";
            query.Dimensions = "ga:source";
            query.GAStartDate = "2005-01-01";
            query.GAEndDate = "2012-01-01";

            try
            {
                feed = this.service.Query(query);
            }
            catch (InvalidCredentialsException e)
            {
                MessageBox.Show("Invalid Analytics username or password");
                //log.Error(e.Message);
                //log.Error(e.StackTrace);
                return link_profile;
            }

            //build Search Details
            foreach (var atom_entry in feed.Entries)
            {
                DataEntry entry = atom_entry as DataEntry;
                foreach (Dimension dimension in entry.Dimensions)
                {
                    link_profile.sites.Add(dimension.Value, 1);
                }
               
            }            

            return link_profile;
        }
    }
}
