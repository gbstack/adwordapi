﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;
using System.Windows;

using Google.GData.WebmasterTools;
using Google.GData.Client;
using Google.Apis.Analytics.v3.Data;

namespace AdwordsScraperGUI.Core
{
    // a website wrapper class for Webproperty(Analytics API) or SitesEntry(Webmaster API)
    public class Website
    {
        string name;
        string url;
        public string sites_entry_url;
        public Webproperty web_property = null;
        public SitesEntry sites_entry = null;
        public bool hasSitesEntry = false;
        public List<KeywordList> keyword_lists;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public string URL
        {
            get { return this.url; }
            set { this.url = value; }
        }

        public Website() {
            if (this.keyword_lists == null)
            {
                this.keyword_lists = new List<KeywordList>();
            }
        }

        public Website getFromWebProperty(Webproperty property)
        {
            this.name = property.Name;
            this.url = property.WebsiteUrl;
            this.web_property = property;

            return this;
        }

        public Website getFromSitesEntry(SitesEntry entry)
        {
            this.name = entry.Title.Text;
            this.url = entry.Title.Text;
            this.sites_entry_url = entry.Title.Text;
            this.sites_entry = entry;

            this.hasSitesEntry = true;

            return this;
        }

        public static List<Website> mergeWebsites(List<Website> websites){
            List<Website> merged_websites = new List<Website>();
            // whether the nth website has been merged.
            bool[] merged = new bool[websites.Count];
            for (int i = 0; i < merged.Length; i++)
            {
                merged[i] = false;
            }

            // merege websites if their URLs are same
            for (int i = 0; i < websites.Count; i++)
            {
                // if this website has been merged, stop and jump to next round of loop
                if (merged[i])
                    continue;

                for (int j = i + 1; j < websites.Count; j++)
                {
                    // merge websites if their url is same
                    //if (websites[i].url == websites[j].url)
                    if(websites[i].url.Contains(websites[j].url) || websites[j].url.Contains(websites[i].url))
                    {
                        // a Webproperty website(Analytics)
                        if(websites[j].web_property != null)
                        {
                            websites[i].getFromWebProperty(websites[j].web_property);
                            merged[j] = true;
                        }
                        // a SitesEntry website(Webmaster)
                        else if (websites[j].hasSitesEntry)
                        {
                            websites[i].getFromSitesEntry(websites[j].sites_entry);
                            merged[j] = true;
                        }
                    }
                }
                merged_websites.Add(websites[i]);
            }

            return merged_websites;
        }

        public WebsiteDetail getDetails(AnalyticsAPI analyticsAPI, WebmasterToolAPI webmasterAPI)
        {
            WebsiteDetail detail = null;
            try
            {
                if (this.web_property != null && !this.hasSitesEntry)
                {
                    AnalyticsReportParameter param = new AnalyticsReportParameter("2011-01-01", "2012-01-01");
                    detail = analyticsAPI.getWebsiteDetails(this, param);
                }
                else if (this.web_property == null && this.hasSitesEntry)
                {
                    detail = webmasterAPI.getWebsiteDetails(this);
                }
                else if (this.web_property != null && this.hasSitesEntry)
                {
                    AnalyticsReportParameter param = new AnalyticsReportParameter("2011-01-01", "2012-01-01");
                    WebsiteDetail detail_analytics = analyticsAPI.getWebsiteDetails(this, param);
                    WebsiteDetail detail_webmaster = webmasterAPI.getWebsiteDetails(this);
                    foreach (KeyValuePair<string, string> kvp in detail_webmaster.attributes)
                    {
                        detail_analytics.attributes[kvp.Key] = kvp.Value;
                    }
                    detail = detail_analytics;
                }
                else if (this.web_property == null && !this.hasSitesEntry)
                {
                    detail = new WebsiteDetail();
                    detail.error_info = "Manual Site";
                }
                
            }
            catch (Exception e)
            {
                // only webmaster API is using GData API, if a site is not verified, a GDataRequestException will be thorwn.
                if (e is GDataRequestException)
                {
                    detail = new WebsiteDetail();
                    detail.error_info = "Not verified";
                }
                else
                {
                    //MessageBox.Show(e.Message);
                }
            }

            return detail;
        }

        public void getLinkProfile()
        {

        }

        public string serialize()
        {
            // TargetInvocationException was generated when using json to serialize SitesEntry
            // And WebmasterAPI::getWebsiteDetails() only use Website's URL, so we can remove Website's sites_entry temporarily
            SitesEntry temp_entry = this.sites_entry;
            this.sites_entry = null;
            string text = new JavaScriptSerializer().Serialize(this);
            this.sites_entry = temp_entry;

            return text;
        }

        public static string saveWebsites(List<Website> websites)
        {
            return Website.saveWebsites(websites, "websites");
        }

        public static string saveWebsites(List<Website> websites, string filename)
        {
            List<string> websites_json = new List<string>();
            // serialization
            foreach (Website website in websites)
            {
                websites_json.Add(website.serialize());
            }
            string text = "[" + String.Join(",", websites_json) + "]";

            StreamWriter writer = new StreamWriter(filename);
            writer.Write(text);
            writer.Close();

            return text;
        }

        public static List<Website> loadWebsites()
        {
            return Website.loadWebsites("websites");
        }

        public static List<Website> loadWebsites(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);
            string text = sr.ReadToEnd();
            sr.Close();
            fs.Close();

            List<Website> websites;
            if (text != "")
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                websites = (List<Website>)serializer.Deserialize(text, typeof(List<Website>));
            }
            else
            {
                websites = new List<Website>();
            }

            foreach (Website site in websites)
            {
                if (site.keyword_lists == null)
                {
                    site.keyword_lists = new List<KeywordList>();
                }
            }

            return websites;
        }

    }

    // website's analytics details or webmaster details
    public class WebsiteDetail
    {
        public Dictionary<string, string> attributes;
        public Dictionary<string, string> attr_names;
        public string error_info = null;

        public WebsiteDetail()
        {
            this.attributes = new Dictionary<string, string>();
            this.attr_names = new Dictionary<string, string>();

            this.attr_names["ga:visits"] = "Visits:";
            this.attr_names["ga:pageviews"] = "Page Views:";
            this.attr_names["ga:newVisits"] = "New Visits:";
            this.attr_names["ga:avgTimeOnSite"] = "Average Time on Site:";
            this.attr_names["ga:visitBounceRate"] = "Visit Bounce Rate:";
            this.attr_names["ga:visitors"] = "Visitors:";
            this.attr_names["not-found"] = "Not Found:";
            this.attr_names["unreachable"] = "Unreachable:";
        }

        public void calculatePagesPerVisit()
        {
            this.attributes["Pages/Visit:"] =
                (Convert.ToDouble(this.attributes["ga:pageviews"]) / Convert.ToDouble(this.attributes["ga:visits"])).ToString();
        }
    }

    public class KeywordList
    {
        public string name;
        public List<string> keywords;
    }
}
