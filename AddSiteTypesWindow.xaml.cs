﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdwordsScraperGUI.Core;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for AddSiteTypesWindow.xaml
    /// </summary>
    public partial class AddSiteTypesWindow : Window
    {
        private Mediator mediator;

        public AddSiteTypesWindow(Mediator mediator)
        {
            InitializeComponent();
            this.mediator = mediator;
            this.Show();
        }

        private void addWebsiteButton_Click(object sender, RoutedEventArgs e)
        {
            this.mediator.createAddWebsiteWindow(this.mediator.parent_window);
            this.Close();
        }

        private void addManualSiteButton_Click(object sender, RoutedEventArgs e)
        {
            this.mediator.createAddManualSiteWindow(this.mediator.parent_window);
            this.Close();
        }
    }
}
