﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201109;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows;

using System.Web.Script.Serialization;
using System.Text.RegularExpressions;

namespace AdwordsScraper
{

    class Idea
    {
        public string keyword;
        public long avgLocalMonthlySearches;
        public long globalMonthlySearches;
        public double competition;
        public string competition_text;
        public long cpc;
        public MonthlySearchVolume[] searchTrends;

        public string Keyword
        {
            get { return keyword; }
            set { keyword = value; }
        }

        public string Competition
        {
            get { return competition_text + "(" + competition.ToString() + ")"; }
        }

        public long LocalSearches
        {
            get { return avgLocalMonthlySearches; }
        }

        public long GlobalSearches
        {
            get { return globalMonthlySearches; }
        }

        public long CPC
        {
            get { return cpc; }
        }

        public string SearchTrends
        {
            get
            {
                string trends_text = "";
                foreach (MonthlySearchVolume monthlySearch in this.searchTrends)
                {
                    trends_text += "year:"+monthlySearch.year.ToString()+" month:"+monthlySearch.month.ToString()+" count:"+monthlySearch.count.ToString()+"\n";
                }
                return trends_text;
            }
        }
    }

    class Statistics
    {
        public Dictionary<string, int> ApiUnitsCost;
        public int languageIndex;
        public int countryIndex;

        public Statistics()
        {
            this.ApiUnitsCost = new Dictionary<string, int>();
        }

        public static Statistics load()
        {
            Statistics statistics;
            FileStream fs = new FileStream("statistics", FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);
            string statistics_text = sr.ReadToEnd();
            fs.Close();

            // if statistics doesn't exits, create new one
            if (statistics_text != "")
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                statistics = (Statistics)serializer.Deserialize(statistics_text, typeof(Statistics));
            }
            else
            {
                statistics = new Statistics();
                string dateNowText = DateTime.Today.Year + "-" + DateTime.Today.Month + "-" + DateTime.Today.Day;
                statistics.ApiUnitsCost.Add(dateNowText, 0);
                statistics.languageIndex = 1;
                statistics.countryIndex = 1;
            }
            return statistics;
        }


        public Statistics useApiUnits(int ApiUnitsUsed)
        {
            string dateNowText = DateTime.Today.Year + "-" + DateTime.Today.Month + "-" + DateTime.Today.Day;
            if (!this.ApiUnitsCost.ContainsKey(dateNowText))
            {
                this.ApiUnitsCost.Add(dateNowText, 0);
            }
            this.ApiUnitsCost[dateNowText] += ApiUnitsUsed;

            return this;
        }

        public Statistics changeLanguageIndex(int languageIndex)
        {
            this.languageIndex = languageIndex;
            return this;
        }

        public Statistics changeCountryIndex(int countryIndex)
        {
            this.countryIndex = countryIndex;
            return this;
        }

        public void save()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            StreamWriter sw = new StreamWriter("statistics");
            sw.Write(serializer.Serialize(this));
            sw.Close();
        }

        public int getUnitsCostThisMonth()
        {
            int cost = 0;

            string pattern = DateTime.Today.Year + "-" + DateTime.Today.Month + "-*";
            Regex regex = new Regex(pattern);
            foreach (KeyValuePair<string, int> kvp in this.ApiUnitsCost)
            {
                if (regex.IsMatch(kvp.Key))
                {
                    cost += kvp.Value;
                }
            }
            return cost;
        }
    }

    class AdwordsScraper
    {
        private TargetingIdeaSelector selector;

        private List<Keyword> keywords;
        private List<KeywordMatchType> matchTypes;
        private Language language;
        private Location location;

        Dictionary<string, bool> usingServices;
        int numberOfResults;
        int ApiUnitsUsed;

        private KeywordMatchTypeSearchParameter keywordMatchTypeParameter;
        private RelatedToKeywordSearchParameter keywordParameter;
        private RelatedToUrlSearchParameter websiteParameter;
        private LanguageSearchParameter languageParameter;
        private LocationSearchParameter locationParameter;

        // selector paging (required for TargetingIdeaService)
        private Paging paging;

        public AdwordsScraper(int pagingStartIndex, int pagingResultsNumber, Dictionary<string, bool> usingServices)
        {
            this.keywords = new List<Keyword>();
            this.matchTypes = new List<KeywordMatchType>();
            this.language = new Language();
            this.location = new Location();
            this.paging = new Paging();
            this.selector = new TargetingIdeaSelector();

            this.keywordMatchTypeParameter = new KeywordMatchTypeSearchParameter();
            this.keywordParameter = new RelatedToKeywordSearchParameter();
            this.websiteParameter = new RelatedToUrlSearchParameter();
            this.languageParameter = new LanguageSearchParameter();
            this.locationParameter = new LocationSearchParameter();


            this.paging.numberResults = pagingResultsNumber;
            this.paging.startIndex = pagingStartIndex;

            this.numberOfResults = pagingResultsNumber;
            this.usingServices = usingServices;
            this.ApiUnitsUsed = AdwordsScraper.calculateApiUnits(this.usingServices, this.numberOfResults);
            
            
        }    

        public static int calculateApiUnits(Dictionary<string, bool> usingServices, int numberOfResults)
        {
            int costUnits = 0;
            if (usingServices["TargetingIdea"])
            {
                // (http://code.google.com/apis/adwords/docs/ratesheet.html) rounded to nearest integer
                costUnits += (int)System.Math.Round(0.1 * numberOfResults) + 5; 
            }
            if (usingServices["TrafficEstimator"])
            {
                costUnits += 15 * numberOfResults + 5;
            }

            if (numberOfResults == 0)
            {
                costUnits = 0;
            }

            return costUnits;
        }

        public static double calculatePrice(int costUnits)
        {
            return (double)costUnits/1000*0.25;
        }

        // These CSV file has 2 or 3 columns, we only use 1st column(description text) and the last column(id)
        public static Dictionary<string, uint> parseCsvCode(string filename)
        {
            Dictionary<string, uint> codes = new Dictionary<string, uint>();
            foreach (string line in System.IO.File.ReadAllLines(filename))
            {
                codes.Add(
                    line.Split(',')[0],
                    Convert.ToUInt32(line.Split(',')[line.Split(',').Length-1])  // last element
                );
            }
            return codes;
        }

        public static Dictionary<string,long> getCPC(List<string> keywords_str)
        {
            TrafficEstimatorService service = (TrafficEstimatorService) new AdWordsUser().GetService(AdWordsService.v201109.TrafficEstimatorService);
            Dictionary<string, long> keywords_cpc = new Dictionary<string, long>();

            List<KeywordEstimateRequest> keyword_requests = new List<KeywordEstimateRequest>();
            foreach (string keyword_str in keywords_str)
            {
                Keyword keyword = new Keyword();
                keyword.text = keyword_str;
                keyword.matchType = KeywordMatchType.BROAD;

                KeywordEstimateRequest keyword_estimate_request = new KeywordEstimateRequest();
                keyword_estimate_request.keyword = keyword;
                keyword_requests.Add(keyword_estimate_request);
            }

            AdGroupEstimateRequest adgroup_request = new AdGroupEstimateRequest();
            adgroup_request.keywordEstimateRequests = keyword_requests.ToArray();
            adgroup_request.maxCpc = new Money();
            adgroup_request.maxCpc.microAmount = 1000000;

            CampaignEstimateRequest campaign_request = new CampaignEstimateRequest();
            campaign_request.adGroupEstimateRequests = new AdGroupEstimateRequest[] { adgroup_request };

            TrafficEstimatorSelector selector = new TrafficEstimatorSelector();
            selector.campaignEstimateRequests = new CampaignEstimateRequest[] { campaign_request };

            try
            {
                TrafficEstimatorResult result = service.get(selector);
                if (result != null && result.campaignEstimates != null && result.campaignEstimates.Length > 0
                    && result.campaignEstimates[0].adGroupEstimates != null && result.campaignEstimates[0].adGroupEstimates.Length > 0
                    && result.campaignEstimates[0].adGroupEstimates[0].keywordEstimates != null)
                {
                    AdGroupEstimate adgroup_estimate = result.campaignEstimates[0].adGroupEstimates[0];
                    for (int i = 0; i < adgroup_estimate.keywordEstimates.Length; i++)
                    {
                        KeywordEstimate keyword_estimate = adgroup_estimate.keywordEstimates[i];
                        long CPC = (keyword_estimate.min.averageCpc.microAmount + keyword_estimate.max.averageCpc.microAmount) / 2;
                        keywords_cpc.Add(keyword_requests[i].keyword.text, CPC);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            return keywords_cpc ;
        }
        /*
        public AdwordsScraper getInput()
        {
            Console.WriteLine("Input the keyword:");
            this.keyword.text = Console.ReadLine();
            this.keyword.text = this.keyword.text.Length == 0 ? "google" : this.keyword.text;

            Console.WriteLine("Input the website:");
            string url = Console.ReadLine();
            this.websiteParameter.urls = new string[] { url.Length == 0 ? "google.com" : url };

            Console.WriteLine("Select the keyword match type:");
            Console.WriteLine("1.Broad");
            Console.WriteLine("2.Exact");
            Console.WriteLine("3.Phrase");
            switch (Console.ReadLine())
            {
                case "1":
                    this.matchType = KeywordMatchType.BROAD;
                    break;
                case "2":
                    this.matchType = KeywordMatchType.EXACT;
                    break;
                case "3":
                    this.matchType = KeywordMatchType.PHRASE;
                    break;
                default:
                    this.matchType = KeywordMatchType.BROAD;
                    break;
            }
            this.keyword.matchType = this.matchType;
            this.keywordMatchTypeParameter.keywordMatchTypes = new KeywordMatchType[] { this.matchType };

            this.keywordParameter.keywords = new Keyword[] { this.keyword };


            return this;
        }
         */

        public AdwordsScraper getInput(string[] keywords, string url, string[] matchTypes, uint languageID, uint locationID)
        {
            // set keywords list
            if (keywords.Length == 0)
            {

                this.keywords = new List<Keyword> { new Keyword() };
                this.keywords[0].text = "google";
            }
            else
            {
                foreach (string keyword_text in keywords)
                {
                    Keyword keyword = new Keyword();
                    keyword.text = keyword_text;
                    this.keywords.Add(keyword);
                }
            }

            //set matchTypes list
            foreach (string matchType_text in matchTypes)
            {
                switch (matchType_text)
                {
                    case "Broad":
                        this.matchTypes.Add(KeywordMatchType.BROAD);
                        break;
                    case "Exact":
                        this.matchTypes.Add(KeywordMatchType.EXACT);
                        break;
                    case "Phrase":
                        this.matchTypes.Add(KeywordMatchType.PHRASE);
                        break;
                    default:
                        break;
                }
            }
            
            foreach (Keyword keyword in this.keywords)
            {
                keyword.matchType = KeywordMatchType.BROAD;
            }
            
            this.language.id = languageID;
            this.location.id = locationID;

            this.websiteParameter.urls = new string[] { url.Length == 0 ? "google.com" : url };
            this.keywordMatchTypeParameter.keywordMatchTypes = this.matchTypes.ToArray();
            this.keywordParameter.keywords = this.keywords.ToArray();
            this.languageParameter.languages = new Language[] { this.language };
            this.locationParameter.locations = new Location[] { this.location };


            return this;
        }

        // setup selector
        public AdwordsScraper setupSelector()
        {

            this.selector.requestType = RequestType.IDEAS;
            this.selector.ideaType = IdeaType.KEYWORD;
            this.selector.requestedAttributeTypes = new AttributeType[] {AttributeType.CRITERION,
                AttributeType.AVERAGE_TARGETED_MONTHLY_SEARCHES, AttributeType.GLOBAL_MONTHLY_SEARCHES, AttributeType.COMPETITION,
                AttributeType.TARGETED_MONTHLY_SEARCHES};
            this.selector.paging = this.paging;

            // set parameters for selector
            this.selector.searchParameters = 
                new SearchParameter[] { 
                    this.keywordParameter, 
                    this.keywordMatchTypeParameter, 
                    this.websiteParameter,
                    this.languageParameter,
                    this.locationParameter
                };

            return this;
        }

        // return a List of Idea
        public List<Idea> run()
        {
            AdWordsUser user = new AdWordsUser();
            TargetingIdeaService service = (TargetingIdeaService)user.GetService(AdWordsService.v201109.TargetingIdeaService);
            List<Idea> ideasList = new List<Idea>();

            try
            {
                TargetingIdeaPage page = service.get(this.selector);

                // whether found related keywords
                if (page != null && page.entries != null)
                {
                    // handle each idea
                    foreach (TargetingIdea idea in page.entries)
                    {
                        // handle every attribute
                        Idea handledIdea = new Idea();
                        foreach (Type_AttributeMapEntry entry in idea.data)
                        {
                            if (entry.key == AttributeType.CRITERION)
                            {
                                CriterionAttribute kwdAttribute = entry.value as CriterionAttribute;
                                switch ((kwdAttribute.value as Keyword).matchType)
                                {
                                    case KeywordMatchType.BROAD:
                                        handledIdea.keyword = (kwdAttribute.value as Keyword).text;
                                        break;
                                    case KeywordMatchType.EXACT:
                                        handledIdea.keyword = "["+(kwdAttribute.value as Keyword).text+"]";
                                        break;
                                    case KeywordMatchType.PHRASE:
                                        handledIdea.keyword = "\""+(kwdAttribute.value as Keyword).text+"\"";
                                        break;
                                    default:
                                        break;
                                }
                            }
                            else if (entry.key == AttributeType.AVERAGE_TARGETED_MONTHLY_SEARCHES)
                            {
                                LongAttribute localSearchAttribute = entry.value as LongAttribute;
                                handledIdea.avgLocalMonthlySearches = localSearchAttribute.value;
                            }
                            else if (entry.key == AttributeType.GLOBAL_MONTHLY_SEARCHES)
                            {
                                LongAttribute globalSearchAttribute = entry.value as LongAttribute;
                                handledIdea.globalMonthlySearches = globalSearchAttribute.value;
                            }
                            else if (entry.key == AttributeType.COMPETITION)
                            {
                                DoubleAttribute competitionAttribute = entry.value as DoubleAttribute;
                                string competition_text;
                                if (competitionAttribute.value >= 0 && competitionAttribute.value <= 0.33)
                                {
                                    competition_text = "Low";
                                }
                                else if (competitionAttribute.value > 0.33 && competitionAttribute.value <= 0.66)
                                {
                                    competition_text = "Medium";
                                }
                                else
                                {
                                    competition_text = "High";
                                }
                                handledIdea.competition = competitionAttribute.value;
                                handledIdea.competition_text = competition_text;
                            }
                            else if (entry.key == AttributeType.TARGETED_MONTHLY_SEARCHES)
                            {
                                handledIdea.searchTrends = (entry.value as MonthlySearchVolumeAttribute).value;
                            }

                        }

                        ideasList.Add(handledIdea);
                    }

                    // update the API units spent
                    Statistics.load().useApiUnits(this.ApiUnitsUsed).save();

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(("Exception:" + e.Message));
            }

            return ideasList;
        }

    }
}

