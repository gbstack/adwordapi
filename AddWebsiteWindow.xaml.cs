﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdwordsScraperGUI.Core;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for AddWebsite.xaml
    /// </summary>
    public partial class AddWebsiteWindow : Window
    {
        IntergratedWindow parent_window;
        AnalyticsAPI analyticsAPI = null;
        WebmasterToolAPI webmasterAPI = null;
        List<Website> websites = new List<Website>();

        public AddWebsiteWindow()
        {
            InitializeComponent();
            
        }
        
        public AddWebsiteWindow(IntergratedWindow parent_window, AnalyticsAPI analyticsAPI, WebmasterToolAPI webmasterAPI)
        {
            InitializeComponent();
            this.parent_window = parent_window;
            this.analyticsAPI = analyticsAPI;
            this.webmasterAPI = webmasterAPI;
            this.buildWebsitesList(Website.loadWebsites("fetched_websites"), false);

            if (this.websites.Count == 0)
            {
                this.refreshWebsitesList();
            }

            this.Show();
        }

        private void buildWebsitesList(List<Website> sites, bool merge = true)
        {
            // websites on Analytics
            GridView gv = new GridView();
            GridViewColumn gvc = new GridViewColumn();
            gvc.Header = "Name";
            gvc.DisplayMemberBinding = new Binding("Name");
            gv.Columns.Add(gvc);

            gvc = new GridViewColumn();
            gvc.Header = "URL";
            gvc.DisplayMemberBinding = new Binding("URL");
            gv.Columns.Add(gvc);

            this.websitesList.View = gv;
            
            if (merge)
            {
                websites = Website.mergeWebsites(sites);
            }
            else
            {
                websites = sites;
            }
            // remove the website if it has already been added to the main window websites list.
            for (int i = websites.Count - 1; i >= 0; i--)
            {
                Website website = websites[i] as Website;

                foreach (Website w in this.parent_window.websites)
                {
                    if (website.URL == w.URL)
                    {
                        websites.RemoveAt(i);
                        break;
                    }
                }
            }
            this.websitesList.DataContext = websites;
        }

        private void addWebsiteButton_Click(object sender, RoutedEventArgs e)
        {
            this.parent_window.mediator.finishAddWebsite(this);
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            refreshWebsitesList();
        }

        private void refreshWebsitesList()
        {
            this.parent_window.loading_text.Text = "Refreshing websites..";
            this.parent_window.updateProgressbar(10);
            Window loadingWindow = new LoadingWindow(this.parent_window);

            List<Website> websites = analyticsAPI.getWebsites().Concat(webmasterAPI.getWebsites()).ToList();
            this.parent_window.updateProgressbar(60);
            this.buildWebsitesList(websites);
            this.parent_window.updateProgressbar(80);
            Website.saveWebsites(Website.mergeWebsites(websites), "fetched_websites");
            this.parent_window.updateProgressbar(100);
            this.parent_window.loading_text.Text = "Done.";

            loadingWindow.Close();
        }

    }
}
