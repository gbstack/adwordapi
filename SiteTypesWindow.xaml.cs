﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for SiteTypesWindow.xaml
    /// </summary>
    public partial class SiteTypesWindow : Window
    {
        public SiteTypesWindow()
        {
            InitializeComponent();

            System.IO.FileStream fs = new System.IO.FileStream(Core.SiteTypes.filename, System.IO.FileMode.OpenOrCreate);
            System.IO.StreamReader sr = new System.IO.StreamReader(fs);
            string text = sr.ReadToEnd();
            sr.Close();
            fs.Close();

            this.sitetypesText.Text = text;

            this.Show();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter(Core.SiteTypes.filename);
            sw.Write(this.sitetypesText.Text);
            sw.Close();
            MessageBox.Show("Save completed.");
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Core.SiteTypes.sitetypes_url);
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                this.sitetypesText.Text = reader.ReadToEnd();
            }
            catch (Exception except)
            {
                MessageBox.Show("HTTP Connection failed.");
            }
            
        }
    }
}
