﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Text.RegularExpressions;

using AdwordsScraperGUI.Core;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for LinkProfileTemplate.xaml
    /// </summary>
    public partial class LinkProfileTemplate : UserControl
    {
        LinkProfile link_profile;
        public LinkProfileTemplate()
        {
            InitializeComponent();
        }

        public LinkProfileTemplate(LinkProfile link_profile){
            InitializeComponent();
            Dictionary<string, int> types_num = new Dictionary<string, int>();
            SiteTypes site_types = SiteTypes.load();
            this.link_profile = link_profile;

            // initialize count for every type
            foreach (string type in site_types.types_rules.Keys)
            {
                if (!types_num.ContainsKey(type))
                {
                    types_num.Add(type, 0);
                }
            }
            foreach (KeyValuePair<string, string> type_rule in site_types.types_rules)
            {
                Regex regex = new Regex(type_rule.Value);
                foreach (KeyValuePair<string, int> site in link_profile.sites)
                {
                    if (regex.IsMatch(site.Key))
                    {
                        types_num[type_rule.Key] += site.Value;
                    }
                }
            }

            string types_stat = "";
            foreach (KeyValuePair<string, int> kvp in types_num)
            {
                types_stat += kvp.Key + ":" + kvp.Value.ToString() + "\n";
            }

            this.pie_chart.DataContext = types_num;
            this.bar_chart.DataContext = types_num;

            //this.websitesText.Text = string.Join("\n", link_profile.sites.Keys.Concat(link_profile.sites.Values.Select(x => x.ToString())));
            this.buildDetailsView();
        }

        private void buildDetailsView()
        {
            GridView gv = this.detailsView.View as GridView;
            gv.Columns.Add(new GridViewColumn() { Header = "URL", DisplayMemberBinding = new Binding("URL"), Width = 200 });
            gv.Columns.Add(new GridViewColumn() { Header = "Count", DisplayMemberBinding = new Binding("Count") });

            var sites = from kvp in this.link_profile.sites select new { URL = kvp.Key, Count = kvp.Value.ToString() };
            this.detailsView.DataContext = sites;
        }
    }
}
