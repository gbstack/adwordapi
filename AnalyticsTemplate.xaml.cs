﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;

using AdwordsScraperGUI.Core;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for AnalyticsTemplate.xaml
    /// </summary>
    public partial class AnalyticsTemplate : UserControl
    {
        public AnalyticsTemplate()
        {
            InitializeComponent();
        }

        public AnalyticsTemplate(Website website, Core.WebsiteDetail detail){
            InitializeComponent();
            this.buildUI(website, detail);
        }

        // iterate the WebsiteDetail's attributes to build the UI
        private void buildUI(Website website, Core.WebsiteDetail detail)
        {
            if (detail != null)
            {
                if (detail.error_info != null)
                {
                    Label error_label = new Label();
                    error_label.Content = detail.error_info;
                    error_label.FontSize = 20;
                    this.rootPanel.Children.Add(error_label);
                }
                foreach (KeyValuePair<string, string> kvp in detail.attributes)
                {
                    StackPanel panel = new StackPanel();
                    panel.Orientation = System.Windows.Controls.Orientation.Horizontal;

                    Label text_label = new Label();
                    if (detail.attr_names.ContainsKey(kvp.Key))
                    {
                        text_label.Content = detail.attr_names[kvp.Key];
                    }
                    else
                    {
                        text_label.Content = kvp.Key;
                    }
                    Label value_label = new Label();
                    value_label.Content = kvp.Value;

                    panel.Children.Add(text_label);
                    panel.Children.Add(value_label);
                    this.rootPanel.Children.Add(panel);
                }
            }
        }
    }
}
