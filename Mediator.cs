﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Windows.Media;
using System.Threading;

using AdwordsScraperGUI.Core;

namespace AdwordsScraperGUI
{
    public class Mediator
    {
        public IntergratedWindow parent_window;
        LoadingWindow loading_window;
        Action<DependencyProperty, double> updateProgressbarValue = null;

        public Mediator() { }
        public Mediator(IntergratedWindow window){
            this.parent_window = window; 
            this.updateProgressbarValue = (dp, val) => this.parent_window.progress_bar.SetValue(dp, val);
        }

        public void updateProgressbar(double value)
        {
            this.parent_window.Dispatcher.Invoke(updateProgressbarValue,
                System.Windows.Threading.DispatcherPriority.Background,
                ProgressBar.ValueProperty,
                value
            );
        }

        public void createAddWebsiteWindow(IntergratedWindow window)
        {
            this.parent_window = window;
            if (Credential.isEmptyCredential())
            {
                MessageBox.Show("some credential field is empty.");
                return;
            }

            //this.loading_window = new LoadingWindow(this.parent_window);
            
            this.parent_window.initApiIfNotExist();
            new AddWebsiteWindow(this.parent_window, this.parent_window.analyticsAPI, this.parent_window.webmasterAPI);

            //this.loading_window.Close();
        }

        public void finishAddWebsite(AddWebsiteWindow window)
        {
            for(int i = 0;i < window.websitesList.SelectedItems.Count;i++)
            {
                Website website = window.websitesList.SelectedItems[i] as Website;

                // check if the website has been added to the list
                bool is_repeat = false;
                foreach (Website w in this.parent_window.websites)
                {
                    if (website.URL == w.URL)
                    {
                        is_repeat = true;
                        break;
                    }
                }
                if (!is_repeat)
                {
                    this.parent_window.websites.Add(website);
                    this.parent_window.buildWebsitesList();
                    Website.saveWebsites(this.parent_window.websites);
                }
                else
                {
                    MessageBox.Show("This website is already in the websites list");
                }
            }

            window.Close();
        }


        public void createAddManualSiteWindow(IntergratedWindow window)
        {
            this.parent_window = window;
            new AddManualSiteWindow(this);
        }

        public void finishAddManualSite(AddManualSiteWindow window)
        {
            try
            {
                Website website = new Website();
                website.Name = window.nameText.Text;
                website.URL = window.urlText.Text;

                if (website.Name == "")
                {
                    throw new Exception("Please input the website name");
                }

                // if duplicate website exists
                foreach (Website w in parent_window.getWebsitesList())
                {
                    if (website.URL == w.URL)
                    {
                        throw new Exception("This website has been added to the list");
                    }
                }
                this.parent_window.getWebsitesList().Add(website);
                this.parent_window.buildWebsitesList();

                Website.saveWebsites(this.parent_window.getWebsitesList());

                window.nameText.Text = "";
                window.urlText.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);             
            }
        }


        public void createAddSiteTypesWindow(IntergratedWindow window)
        {
            this.parent_window = window;
            new AddSiteTypesWindow(this);
        }


        public void clickWebsiteItem(Website website)
        {
            this.parent_window.loading_text.Text = "Loading Website Details";
            this.updateProgressbar(30);

            // set the Adwords site url same as selected site, if the selected site is manual site
            if (website.web_property == null && !website.hasSitesEntry)
            {
                this.parent_window.adwords_grid.websiteText.Text = website.URL;
            }                                           

            if (!hasDuplicatedTab(website.Name, parent_window.contentTabs))
            {
                ClosableTabItem tab = new ClosableTabItem(website.Name);
                tab.Content = 
                    new AnalyticsTemplate(
                        website, 
                        website.getDetails(this.parent_window.analyticsAPI, 
                        this.parent_window.webmasterAPI)
                    );
                tab.IsSelected = true;
                this.parent_window.contentTabs.Items.Add(tab);
            }

            this.updateProgressbar(100);
            this.parent_window.loading_text.Text = "Done.";
        }

        public bool hasDuplicatedTab(string name, TabControl tabs)
        {
            foreach (TabItem t in tabs.Items)
            {
                if (name == ((t.Header as StackPanel).Children[0] as Label).Content.ToString())
                {
                    t.IsSelected = true;
                    return true;
                }
            }

            return false;
        }

        public void clickKeywordListItem()
        {
            this.parent_window.loading_text.Text = "Loading Keywords";
            this.updateProgressbar(20);
            TreeViewItem selected_item = parent_window.websitesList.SelectedItem as TreeViewItem;
            int site_index = TreeViewHelper.getTreeviewSelectedIndex(parent_window.websitesList,
                (selected_item.Parent as TreeViewItem).Parent as TreeViewItem );
            int keyword_list_index = TreeViewHelper.getKeywordListIndex(selected_item.Parent as TreeViewItem, selected_item);
            KeywordList keyword_list = parent_window.websites[site_index].keyword_lists[keyword_list_index];

            Website selected_website = parent_window.websites[site_index];
            this.updateProgressbar(40);

            // new Keyword List Tab
            if (!hasDuplicatedTab(TreeViewHelper.getItemText(selected_item), parent_window.contentTabs))
            {
                ClosableTabItem tab = new ClosableTabItem(selected_website.Name + " > " + keyword_list.name);
                this.updateProgressbar(60);
                tab.Content = new KeywordListTemplate(keyword_list, selected_website,
                    new OldAnalyticsAPI().getSearchDetails(selected_website, parent_window.analyticsAPI,"2007-01-01", "2012-01-01"));
                tab.IsSelected = true;
                parent_window.contentTabs.Items.Add(tab);
            }
            this.updateProgressbar(100);
            this.parent_window.loading_text.Text = "Done";
            
        }

        public void clickLinkProfileItem()
        {
            this.parent_window.loading_text.Text = "Loading LinkProfile";
            this.updateProgressbar(20);
            TreeViewItem selected_item = parent_window.websitesList.SelectedItem as TreeViewItem;
            int site_index = TreeViewHelper.getTreeviewSelectedIndex(parent_window.websitesList,
                selected_item.Parent as TreeViewItem);
            this.updateProgressbar(50);

            LinkProfile link_profile = new LinkProfile();
            
            WebmasterScraper scraper = new WebmasterScraper(this, parent_window.websites[site_index], link_profile);
            scraper.bindEvent(selected_item);
            scraper.start();
            //Thread thread = new Thread(scraper.start);            
            //thread.Start();
            //while (link_profile.sites.Count == 0)
            //{
            //    Thread.Sleep(1000);
            //}
                //new OldAnalyticsAPI().getLinkProfile(parent_window.websites[site_index], parent_window.analyticsAPI);
            //this.updateProgressbar(80);

            // new Link Profile Tab
            //if (!hasDuplicatedTab(TreeViewHelper.getItemText(selected_item), parent_window.contentTabs))
            //{
            //    ClosableTabItem tab = new ClosableTabItem(parent_window.websites[site_index].Name + " Link Profile");
            //    tab.Content = new LinkProfileTemplate(link_profile);
            //    tab.IsSelected = true;
            //    parent_window.contentTabs.Items.Add(tab);
            //}
            //this.updateProgressbar(100);
            //this.parent_window.loading_text.Text = "Done";
            
        }

        public void finishCredentialSettings(CredentialSettings window)
        {
            this.parent_window.refreshApi();
        }

        public void finishAddKeywordList(AddKeywordListWindow window)
        {
            TreeViewItem selected_item = parent_window.websitesList.SelectedItem as TreeViewItem;          
            int site_index = TreeViewHelper.getTreeviewSelectedIndex(parent_window.websitesList, selected_item.Parent as TreeViewItem);

            //check if duplicated keyword list existed
            foreach (var item in selected_item.Items)
            {
                if (window.nameText.Text == TreeViewHelper.getItemText(item as TreeViewItem))
                {
                    throw new DuplicatedKeywordListException();                    
                }
            }
            
            KeywordList keyword_list = new KeywordList();
            keyword_list.name = window.nameText.Text;
            keyword_list.keywords = window.keywordsText.Text.Split(window.delimiter).ToList<string>();            
            
            parent_window.websites[site_index].keyword_lists.Add(keyword_list);
            parent_window.buildWebsitesList();
            Website.saveWebsites(parent_window.websites);
            
            // restore Treeview status
            TreeViewHelper.expandTreeviewItems(parent_window.websitesList.Items[site_index] as TreeViewItem);
        }

        public void finishGotLicense()
        {
            parent_window.Show();
            parent_window.buildUI();
        }
    }
}
        