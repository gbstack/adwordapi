﻿/*
 * Created by SharpDevelop.
 * User: Administrator
 * Date: 01/30/2012
 * Time: 19:24
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace AdwordsScraperGUI
{
	/// <summary>
	/// Interaction logic for WordnetGrid.xaml
	/// </summary>
	public partial class WordnetGrid : UserControl
	{
        Dictionary<string, List<int>> opts_index = new Dictionary<string, List<int>>();
        string[] opt_names = new string[] { "Synonyms", "Coordinate", "Hypernyms", "Familiarity" };
        bool use_morph = true;

		public WordnetGrid()
		{
			InitializeComponent();
            this.buildUI();
		}

        private void buildUI()
        {
            wordnet_grid.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            wordnet_grid.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;

            int column = 0;
            foreach (string opt_name in opt_names)
            {
                Button btn = new Button();               
                btn.Content = opt_name;
                Grid.SetRow(btn, 1);
                Grid.SetColumn(btn, column);
                wordnet_grid.Children.Add(btn);

                btn.Click += delegate(object sender, RoutedEventArgs args)
                {
                    displayResults((sender as Button).Content.ToString());
                };

                column++;
            }
        }

        private void setDictPath()
        {
            System.Windows.Forms.FolderBrowserDialog folder_dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = folder_dialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                this.dictPathText.Text = folder_dialog.SelectedPath + "\\";
            }
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            WnLexicon.WordInfo word_info = null;

            if (this.wordText.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please input the word");
                return;
            }
            if (this.dictPathText.Text.Trim().Length == 0)
            {
                setDictPath();
            }

            try
            {
                Wnlib.WNCommon.path = this.dictPathText.Text.Trim();
                word_info = WnLexicon.Lexicon.FindWordInfo(this.wordText.Text, use_morph);
            }
            catch (TypeInitializationException except)
            {
                MessageBox.Show("Please set the correct dictionary path\ne.g.\nC:\\Program Files\\WordNet\\2.1\\dict\\");
                return;
            }
            if (word_info.partOfSpeech == Wnlib.PartsOfSpeech.Unknown)
            {
                this.resultText.Text = "No match found.";
                return;
            }

            Wnlib.Opt opt = null;
            for (int i = 0; i < Wnlib.Opt.Count; i++)
            {
                opt = Wnlib.Opt.at(i);
                foreach (string key in opt_names)
                {
                    if (opt.label.IndexOf(key) > -1)
                    {
                        if (!opts_index.ContainsKey(key))
                        {
                            opts_index.Add(key, new List<int>());
                        }
                        opts_index[key].Add(i);
                    }
                }
            }
            displayResults("Synonyms");
        }
        
        private void displayResults(string opt_name){
            Wnlib.Opt opt = null;
            this.resultText.Text = "";

            if (opts_index.Count == 0)
            {
                this.resultText.Text = "No search.";
                return;
            }

            foreach (int index in opts_index[opt_name])
            {
                opt = Wnlib.Opt.at(index);
                //Wnlib.Search search = new Wnlib.Search(this.wordText.Text, use_morph, Wnlib.PartOfSpeech.of(Wnlib.PartsOfSpeech.Noun), opt.sch, Int16.Parse("0"));
                Wnlib.Search search = new Wnlib.Search(this.wordText.Text, use_morph, opt.pos, opt.sch, Int16.Parse("0"));
                if (search.buf != "")
                {                    
                    this.resultText.Text += search.buf;
                }
            }
            
        }

        private void selectButton_Click(object sender, RoutedEventArgs e)
        {
            setDictPath();
        }
	}
}