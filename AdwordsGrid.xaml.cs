﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for AdwordsGrid.xaml
    /// </summary>


    public partial class AdwordsGrid : UserControl
    {

        private Dictionary<string, uint> langcodes;
        private Dictionary<string, uint> countrycodes;
        private List<string> matchTypes;
        private int numberOfResults;
        private int pageSize;
        private Dictionary<string, bool> usingServices;

        private LoadingWindow loadingWindow;

        public int LanguageIndex
        {
            get
            {
                return this.languageSelect.SelectedIndex;
            }
            set
            {
                this.languageSelect.SelectedIndex = value;
            }
        }

        public int CountryIndex
        {
            get
            {
                return this.countrySelect.SelectedIndex;
            }
            set
            {
                this.countrySelect.SelectedIndex = value;
            }
        }

        public AdwordsGrid()
        {
            InitializeComponent();
            this.matchTypes = new List<string>();
            this.usingServices = new Dictionary<string, bool>();

            // binding data to combobox
            this.langcodes = AdwordsScraper.AdwordsScraper.parseCsvCode("data/languagecodes.csv");
            this.languageSelect.DataContext = this.langcodes.Keys;
            this.countrycodes = AdwordsScraper.AdwordsScraper.parseCsvCode("data/countrycodes.csv");
            this.countrySelect.DataContext = this.countrycodes.Keys;

            this.pageSize = 10;
            this.numberOfResults = this.pageSize;

            this.usingServices.Add("TargetingIdea", (bool)this.showIdeas.IsChecked);
            // the showCPC checkbox is already initialized.
            this.usingServices.Add("TrafficEstimator", (bool)this.showCPC.IsChecked);

            // UI
            this.showIdeas.IsChecked = true;

            this.updateCost();

            AdwordsScraper.Statistics statistics = AdwordsScraper.Statistics.load();
            this.totalCostLabel.Content = statistics.getUnitsCostThisMonth();
            this.languageSelect.SelectedIndex = statistics.languageIndex;
            this.countrySelect.SelectedIndex = statistics.countryIndex;

            TabItem wordnet_tab = new TabItem();
            wordnet_tab.Content = new WordnetGrid();
            wordnet_tab.Header = "WordNet";
            this.adwordsTabs.Items.Add(wordnet_tab);
        }

        private void setLoadingText(string text)
        {
            IntergratedWindow main_window;
            if ((main_window = ((this.Parent as Grid).Parent as IntergratedWindow)) != null)
            {
                main_window.loading_text.Text = text;
            }
        }

        private void updateProgressBar(double value)
        {
            IntergratedWindow main_window;
            if ((main_window = ((this.Parent as Grid).Parent as IntergratedWindow)) != null)
            {
                main_window.updateProgressbar(value);
            }
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.matchTypes.Count == 0)
                {
                    this.matchTypes = new List<string> { "Broad", "Exact", "Phrase" };
                }

                if (this.usingServices["TrafficEstimator"] == false && this.usingServices["TargetingIdea"] == false)
                {
                    MessageBox.Show("Please select at least one service");
                }
                else
                {
                    //this.showDebugInfo();

                    
                    if (this.keywordText.Text != "")
                    {
                        this.setLoadingText("loading adwords keywords");
                        this.updateProgressBar(10);
                        this.loadingWindow = new LoadingWindow();
                        //loadingWindow.Left = this.Left + (this.Width - loadingWindow.Width) / 2;
                        //loadingWindow.Top = this.Top + (this.Height - loadingWindow.Height) / 2;

                        this.loadingWindow.Show();

                        this.startScraper();

                        this.loadingWindow.Close();
                        this.updateProgressBar(100);
                        this.setLoadingText("Done");
                    }
                    else
                    {
                        MessageBox.Show("Please input at least one keyword");
                    }
                }
            }
            catch (Exception except)
            {
                MessageBox.Show(except.Message);
                MessageBox.Show(except.StackTrace);
            }
        }

        private void startScraper()
        {
            List<AdwordsScraper.Idea> ideas;

            if (this.usingServices["TargetingIdea"])
            {
                this.loadingWindow.LoadingText += "\nGetting keyword ideas...";
                AdwordsScraper.AdwordsScraper scraper = new AdwordsScraper.AdwordsScraper(0, this.numberOfResults, this.usingServices);
                this.updateProgressBar(20);

                scraper.getInput(
                    this.keywordText.Text.Split(','),
                    this.websiteText.Text,
                    this.matchTypes.ToArray(),
                    this.langcodes[this.languageSelect.SelectedItem.ToString()],
                    this.countrycodes[this.countrySelect.SelectedItem.ToString()]
                );
                this.updateProgressBar(40);

                scraper.setupSelector();
                this.updateProgressBar(60);

                ideas = scraper.run();
                this.loadingWindow.LoadingText += "\nGetting CPCs...";
                if (this.usingServices["TrafficEstimator"])
                {
                    ideas = this.getCPC(ideas);
                }
                this.updateProgressBar(90);
            }
            else
            {
                ideas = new List<AdwordsScraper.Idea>();
                List<string> keywords_list = this.keywordText.Text.Split(',').ToList();
                for (int i = keywords_list.Count - 1; i >= 0; i--)
                {
                    if (keywords_list[i].Trim() == "")
                        keywords_list.Remove(keywords_list[i]);
                }
                this.loadingWindow.LoadingText += "\nGetting CPCs...";
                ideas = this.getCPC(keywords_list.ToArray());
            }



            // bind ideas to listview
            List<string> columnNames = new List<string> { "Keyword", "Competition", "LocalSearches", "GlobalSearches", "CPC", "SearchTrends" };
            if (this.usingServices["TrafficEstimator"] == false)
            {
                columnNames.Remove("CPC");
            }
            else if (this.usingServices["TrafficEstimator"] == true && this.usingServices["TargetingIdea"] == false)
            {
                columnNames = new List<string> { "Keyword", "CPC" };
            }

            GridView gv = new GridView();
            foreach (string columnName in columnNames)
            {
                GridViewColumn gvc = new GridViewColumn();
                gvc.Header = columnName;
                gvc.DisplayMemberBinding = new Binding(columnName);
                gv.Columns.Add(gvc);
            }

            this.ideasList.View = gv;
            this.ideasList.DataContext = ideas;

            // update the units cost after searching
            this.totalCostLabel.Content = AdwordsScraper.Statistics.load().getUnitsCostThisMonth();

        }

        private List<AdwordsScraper.Idea> getCPC(List<AdwordsScraper.Idea> ideas)
        {
            List<string> keywords_str = new List<string>();

            for (int i = 0; i < ideas.Count; i++)
            {
                keywords_str.Add(ideas[i].keyword);
            }
            Dictionary<string, long> keywords_cpc = AdwordsScraper.AdwordsScraper.getCPC(keywords_str);

            for (int i = 0; i < ideas.Count; i++)
            {
                ideas[i].cpc = keywords_cpc[ideas[i].keyword];
            }

            return ideas;
        }

        private List<AdwordsScraper.Idea> getCPC(string[] keywords)
        {
            List<AdwordsScraper.Idea> ideas = new List<AdwordsScraper.Idea>();
            Dictionary<string, long> keywords_cpc = AdwordsScraper.AdwordsScraper.getCPC(keywords.OfType<string>().ToList());
            for (int i = 0; i < keywords_cpc.Count; i++)
            {
                AdwordsScraper.Idea idea = new AdwordsScraper.Idea();
                idea.keyword = keywords[i];
                idea.cpc = keywords_cpc[keywords[i]];

                ideas.Add(idea);
            }
            return ideas;
        }

        private void showDebugInfo()
        {
            string matchTypes_str = "";
            foreach (string matchtype_text in this.matchTypes)
            {
                matchTypes_str += matchtype_text;
            }
            MessageBox.Show(this.keywordText.Text + " " + this.websiteText.Text + " " + matchTypes_str
                + "\nLanguage id: " + this.langcodes[this.languageSelect.SelectedItem.ToString()]
                + "\nCountry id: " + this.countrycodes[this.countrySelect.SelectedItem.ToString()]
                );
        }



        private void matchType_Exact_Checked(object sender, RoutedEventArgs e)
        {
            this.matchTypes.Add("Exact");
        }

        private void matchType_Phrase_Checked(object sender, RoutedEventArgs e)
        {
            this.matchTypes.Add("Phrase");
        }

        private void matchType_Broad_Checked(object sender, RoutedEventArgs e)
        {
            this.matchTypes.Add("Broad");
        }

        private void matchType_Broad_Unchecked(object sender, RoutedEventArgs e)
        {
            this.matchTypes.Remove("Broad");
        }

        private void matchType_Phrase_Unchecked(object sender, RoutedEventArgs e)
        {
            this.matchTypes.Remove("Phrase");
        }

        private void matchType_Exact_Unchecked(object sender, RoutedEventArgs e)
        {
            this.matchTypes.Remove("Exact");
        }

        private void showCPC_Checked(object sender, RoutedEventArgs e)
        {
            this.usingServices["TrafficEstimator"] = true;
            this.updateCost();
        }

        private void showCPC_Unchecked(object sender, RoutedEventArgs e)
        {
            this.usingServices["TrafficEstimator"] = false;
            this.updateCost();
        }

        private void preferenceButton_Click(object sender, RoutedEventArgs e)
        {
            PreferenceWindow preference = new PreferenceWindow(this);
            preference.Show();
        }

        private void showIdeas_Checked(object sender, RoutedEventArgs e)
        {
            this.usingServices["TargetingIdea"] = true;
            this.updateCost();
        }

        private void showIdeas_Unchecked(object sender, RoutedEventArgs e)
        {
            this.usingServices["TargetingIdea"] = false;
            this.updateCost();
        }

        private void updateCost()
        {
            // update records number
            if (this.usingServices["TargetingIdea"] == false)
            {
                List<string> keywords_list = this.keywordText.Text.Split(',').ToList();
                try
                {
                    for (int i = keywords_list.Count - 1; i >= 0; i--)
                    {
                        if (keywords_list[i].Trim() == "")
                        {
                            keywords_list.Remove(keywords_list[i]);
                            if (keywords_list.Count == 0)
                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                    // Using foreach will change the collection size during the iteration, iteration will abort.
                    MessageBox.Show(e.Message);
                }
                this.numberOfResults = keywords_list.Count;
            }
            else if (this.usingServices["TargetingIdea"] == true)
            {
                this.numberOfResults = this.pageSize;
            }

            // update UI
            int costApiUnits = AdwordsScraper.AdwordsScraper.calculateApiUnits(this.usingServices, this.numberOfResults);
            this.ApiUnitsLabel.Content = costApiUnits;
            this.priceLabel.Content = AdwordsScraper.AdwordsScraper.calculatePrice(costApiUnits);
        }

        private void keywordText_KeyUp(object sender, KeyEventArgs e)
        {
            this.updateCost();
        }

    }
}
