﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AdwordsScraperGUI
{

    /// <summary>
    /// Interaction logic for LoadingWindow.xaml
    /// </summary>
    public partial class LoadingWindow : Window
    {
        public string LoadingText
        {
            get { return this.LoadingLabel.Content.ToString(); }
            set { this.LoadingLabel.Content = value; }
        }

        public LoadingWindow()
        {
            InitializeComponent();
            this.Left = (System.Windows.SystemParameters.PrimaryScreenWidth - this.Width) / 2;
            this.Top = (System.Windows.SystemParameters.PrimaryScreenHeight - this.Height) / 2;
            this.init();
            this.Show();
        }

        public LoadingWindow(Window parent_window)
        {
            InitializeComponent();
            this.Left = parent_window.Left + (parent_window.Width - this.Width) / 2;
            this.Top = parent_window.Top + (parent_window.Height - this.Height) / 2;
            this.init();
            this.Show();
        }

        private void init()
        {
            this.Topmost = true;
        }
    }
}
