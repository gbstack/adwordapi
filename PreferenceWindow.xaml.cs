﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for PreferenceWindow.xaml
    /// </summary>
    public partial class PreferenceWindow : Window
    {

        private Dictionary<string, uint> langcodes;
        private Dictionary<string, uint> countrycodes;
        private string langcodes_path = "data/languagecodes.csv";
        private string countrycodes_path = "data/countrycodes.csv";
        private AdwordsGrid mainWindow;

        public PreferenceWindow(MainWindow mainWindow)
        {
            try
            {
                InitializeComponent();

                //this.mainWindow = mainWindow;
                this.langcodes = AdwordsScraper.AdwordsScraper.parseCsvCode(this.langcodes_path);
                this.languageSelect.DataContext = this.langcodes.Keys;
                this.languageSelect.SelectedIndex = AdwordsScraper.Statistics.load().languageIndex;
                this.countrycodes = AdwordsScraper.AdwordsScraper.parseCsvCode(this.countrycodes_path);
                this.countrySelect.DataContext = this.countrycodes.Keys;
                this.countrySelect.SelectedIndex = AdwordsScraper.Statistics.load().countryIndex;

                this.token.Text = AdwordsScraper.Config.load().attributes["DeveloperToken"];
                this.username.Text = AdwordsScraper.Config.load().attributes["Email"];
                this.password.Text = AdwordsScraper.Config.load().attributes["Password"];
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            
        }

        public PreferenceWindow(AdwordsGrid mainWindow)
        {
            try
            {
                InitializeComponent();

                this.mainWindow = mainWindow;
                this.langcodes = AdwordsScraper.AdwordsScraper.parseCsvCode(this.langcodes_path);
                this.languageSelect.DataContext = this.langcodes.Keys;
                this.languageSelect.SelectedIndex = AdwordsScraper.Statistics.load().languageIndex;
                this.countrycodes = AdwordsScraper.AdwordsScraper.parseCsvCode(this.countrycodes_path);
                this.countrySelect.DataContext = this.countrycodes.Keys;
                this.countrySelect.SelectedIndex = AdwordsScraper.Statistics.load().countryIndex;

                this.token.Text = AdwordsScraper.Config.load().attributes["DeveloperToken"];
                this.username.Text = AdwordsScraper.Config.load().attributes["Email"];
                this.password.Text = AdwordsScraper.Config.load().attributes["Password"];
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }

        private void applyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AdwordsScraper.Statistics.load()
                    .changeLanguageIndex(this.languageSelect.SelectedIndex)
                    .changeCountryIndex(this.countrySelect.SelectedIndex)
                    .save();
                this.mainWindow.LanguageIndex = this.languageSelect.SelectedIndex;
                this.mainWindow.CountryIndex = this.countrySelect.SelectedIndex;

                AdwordsScraper.Config config = AdwordsScraper.Config.load();
                bool restart = false;
                if (config.attributes["DeveloperToken"] != this.token.Text || config.attributes["Email"] != this.username.Text
                    || config.attributes["Password"] != this.password.Text)
                    restart = true;
                config.attributes["DeveloperToken"] = this.token.Text;
                config.attributes["Email"] = this.username.Text;
                config.attributes["Password"] = this.password.Text;
                config.save();

                // does not work
                //System.Configuration.ConfigurationManager.RefreshSection("AdWordsApi");
                if (restart)
                {
                    new RestartWindow();
                }

                this.Close();
            }
            catch (Exception except)
            {
                MessageBox.Show(except.Message);
            }
        }
    }
}
