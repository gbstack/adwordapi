﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using AdwordsScraperGUI.Core;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for KeywordListTemplate.xaml
    /// </summary>
    public partial class KeywordListTemplate : UserControl
    {
        Website website;
        KeywordList keyword_list;

        public KeywordListTemplate()
        {
            InitializeComponent();
        }

        public KeywordListTemplate(KeywordList keyword_list)
        {
            InitializeComponent();
            this.keyword_list = keyword_list;
            this.keywordsText.Text = string.Join(",", keyword_list.keywords);
        }

        public KeywordListTemplate(KeywordList keyword_list, Website website, List<SearchDetail> search_details)
        {
            InitializeComponent();
            this.keyword_list = keyword_list;
            this.keywordsText.Text = string.Join(",", keyword_list.keywords);
            this.website = website;

            //build keywords list view
            this.buildKeywordsList(search_details);
        }

        public void buildKeywordsList(List<SearchDetail> search_details)
        {
            GridView gv = new GridView();
            string[] columns = new string[] { "keyword", "organic_visits", "paid_visits" };
            foreach (string column in columns)
            {
                GridViewColumn gvc = new GridViewColumn();
                gvc.Header = column;
                gvc.DisplayMemberBinding = new Binding(column);
                gv.Columns.Add(gvc);
            }

            this.keywords_listview.View = gv;

            // filter details
            HashSet<SearchDetail> filterd_details = new HashSet<SearchDetail>();
            foreach (SearchDetail detail in search_details)
            {
                foreach (string keyword in keyword_list.keywords)
                {
                    if (detail.keyword == keyword)
                    {
                        filterd_details.Add(detail);
                    }
                }
            }

            this.keywords_listview.DataContext = filterd_details;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (this.start_date.SelectedDate == null || this.end_date.SelectedDate == null)
            {
                MessageBox.Show("Please select a date for start date or end date");
                return;
            }
            else
            {
                LoadingWindow loading_window = new LoadingWindow();
                IntergratedWindow parent_window = (((this.Parent as TabItem).Parent as TabControl).Parent as Grid).Parent as IntergratedWindow;
                List<SearchDetail> search_details = new OldAnalyticsAPI().getSearchDetails(website, parent_window.analyticsAPI, DateTimeHelper.getFormattedOutput((DateTime)start_date.SelectedDate), DateTimeHelper.getFormattedOutput((DateTime)end_date.SelectedDate));
                this.buildKeywordsList(search_details);
                loading_window.Close();
            }
            
        }

        private void saveKeywords()
        {
            // this.keyword_list is reference to keyword lists of parent_window.websites
            this.keyword_list.keywords = this.keywordsText.Text.Split(',').ToList<string>();
            IntergratedWindow parent_window = (((this.Parent as TabItem).Parent as TabControl).Parent as Grid).Parent as IntergratedWindow;
            Website.saveWebsites(parent_window.websites);
            MessageBox.Show("Save complete.");
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            saveKeywords();
        }

        private void keywordsText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                saveKeywords();
            }
        }
    }
}
