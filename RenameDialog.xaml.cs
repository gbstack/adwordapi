﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for RenameDialog.xaml
    /// </summary>
    public partial class RenameDialog : Window
    {
        IntergratedWindow parent_window;
        TreeViewItem selected_item;

        int site_index;
        int keyword_list_index;
        string type;

        public RenameDialog()
        {
            InitializeComponent();
        }

        public RenameDialog(IntergratedWindow parent_window)
        {
            InitializeComponent();
            this.parent_window = parent_window;

            selected_item = parent_window.websitesList.SelectedItem as TreeViewItem;
            type = TreeViewHelper.getTreeviewItemType(selected_item);

            // Website Item
            if(type == "site"){
                this.site_index = TreeViewHelper.getTreeviewSelectedIndex(parent_window.websitesList);       
            }
            // Keyword List Item
            else if (type == "keyword_list")
            {
                this.site_index = TreeViewHelper.getTreeviewSelectedIndex(parent_window.websitesList,
                    ((selected_item.Parent as TreeViewItem).Parent as TreeViewItem));
                this.keyword_list_index =
                    TreeViewHelper.getKeywordListIndex(selected_item.Parent as TreeViewItem, selected_item);    
            }
            this.nameText.Text = TreeViewHelper.getItemText(selected_item);
        
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            if (type == "site")
            {
                this.parent_window.websites[this.site_index].Name = this.nameText.Text;
            }
            else if (type == "keyword_list")
            {
                this.parent_window.websites[this.site_index].keyword_lists[this.keyword_list_index].name = this.nameText.Text;
                TreeViewHelper.expandTreeviewItems(parent_window.websitesList.Items[site_index] as TreeViewItem);
            }

            this.parent_window.buildWebsitesList();
            Core.Website.saveWebsites(this.parent_window.websites);
            
            // restore Treeview
            if (type == "keyword_list")
            {            
                TreeViewHelper.expandTreeviewItems(parent_window.websitesList.Items[site_index] as TreeViewItem);
            }

            this.Close();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
