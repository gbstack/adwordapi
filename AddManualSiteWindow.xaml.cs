﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AdwordsScraperGUI.Core;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for AddManualSiteWindow.xaml
    /// </summary>
    public partial class AddManualSiteWindow : Window
    {
        Mediator mediator;

        public AddManualSiteWindow(Mediator mediator)
        {
            InitializeComponent();
            this.mediator = mediator;
            this.Show();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            this.mediator.finishAddManualSite(this);
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
