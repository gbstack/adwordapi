﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdwordsScraperGUI.Core;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for CredentialSettings.xaml
    /// </summary>
    public partial class CredentialSettings : Window
    {

        Mediator mediator;

        public CredentialSettings(Mediator mediator = null)
        {
            InitializeComponent();
            this.Show();
            this.mediator = mediator;

            Credential credential = Credential.load();
            this.clientKeyText.Text = credential.client_key;
            this.clientSecretText.Text = credential.client_secret;
            this.analytics_usernameText.Text = credential.analytics_username;
            this.analytics_passwordText.Text = credential.analytics_password;
            this.usernameText.Text = credential.username;
            this.passwordText.Text = credential.password;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            Credential credential = new Credential();
            credential.client_key = this.clientKeyText.Text.Trim();
            credential.client_secret = this.clientSecretText.Text.Trim();
            credential.username = this.usernameText.Text.Trim();
            credential.password = this.passwordText.Text.Trim();
            credential.analytics_username = this.analytics_usernameText.Text.Trim();
            credential.analytics_password = this.analytics_passwordText.Text.Trim();
            credential.first_use = Credential.load().first_use;

            if (credential.client_key.Length == 0 || credential.client_secret.Length == 0)
            {
                MessageBox.Show("Client Key or Client Secret is empty. Analytics websites cannot be fetched");
            }
            else
            {
                Credential saved_credential = Credential.load();
                // if oauth credentials are changed, get a new oauth access token
                if (saved_credential.client_key != credential.client_key || saved_credential.client_secret != credential.client_secret)
                {
                    AnalyticsAPI.getNewToken(credential.client_key, credential.client_secret);
                }                
            }
            credential.save();

            if (this.mediator != null)
            {
                this.mediator.finishCredentialSettings(this);
            }

            this.Close();
        }

        private void setLaterButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
