﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using AdwordsScraperGUI.Core;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for AccountSettingsWindow.xaml
    /// </summary>
    public partial class AccountSettingsWindow : Window
    {
        Mediator mediator;

        public AccountSettingsWindow(Mediator mediator)
        {
            LoadingWindow loading_window = new LoadingWindow(this);

            InitializeComponent();
            Account account = Account.load();
            this.usernameText.Text = account.username;
            this.emailText.Text = account.email;
            this.mediator = mediator;
            this.Show();

            loading_window.Close();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            Account account = new Account();
            account.username = this.usernameText.Text.Trim();
            account.email = this.emailText.Text.Trim();

            License license = License.get(License.license_url, account.username);
            account.license = license;

            if (license != null && license.key != null)
            {
                if (License.generateKey(account.username, account.email) == license.key)
                {
                    if (!License.isOutOfDate(License.license_url, account.username))
                    {
                        this.mediator.finishGotLicense();
                        account.save();
                        MessageBox.Show("Account verified. \nLicense expiration:" + license.expiration + "\nLicense type:" + license.type);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("License is out of date.");
                    }
                }
                else
                {
                    MessageBox.Show("invalid account");
                }
            }
            else
            {
                MessageBox.Show("invalid account");
            }
            
        }
    }
}
