﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;


namespace AdwordsScraper
{
    class Config
    {
        public Dictionary<string, string> attributes = new Dictionary<string, string>();

        public Config()
        {
            this.attributes.Add("DeveloperToken", "");
            this.attributes.Add("Email", "");
            this.attributes.Add("Password", "");
        }

        public static Config load()
        {
            XmlTextReader reader = new XmlTextReader("AdwordsScraperGUI.exe.config");
            Config config = new Config();

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:

                        bool isExpectedAttribute = false;
                        string key = "";
                        while (reader.MoveToNextAttribute())
                        {
                            if (isExpectedAttribute)
                            {
                                config.attributes[key] = reader.Value;
                                isExpectedAttribute = false;
                            }
                            if (new string[] { "DeveloperToken", "Email", "Password" }.Contains(reader.Value))
                            {
                                key = reader.Value;
                                isExpectedAttribute = true;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            reader.Close();

            return config;
        }

        public void save()
        {
            // copy app.config because we can't read and write the same file at the same time
            string dst_path = @".\AdwordsScraperGUI.exe.config.bak";
            string src_path = @".\AdwordsScraperGUI.exe.config";
            System.IO.File.Copy(src_path, dst_path, true);

            XmlTextReader reader = new XmlTextReader("AdwordsScraperGUI.exe.config.bak");
            XmlWriter writer = XmlWriter.Create("AdwordsScraperGUI.exe.config");

            writer.WriteStartDocument();
            while (reader.Read())
            {
                bool isSelfEnd = false;
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        writer.WriteStartElement(reader.Name);
                        if (new string[] { "section", "add", "supportedRuntime", "httpWebRequest" }.Contains(reader.Name))
                        {
                            isSelfEnd = true;
                        }

                        // write attributes
                        bool isExpectedAttribute = false;
                        string key = "";
                        while (reader.MoveToNextAttribute())
                        {
                            if (isExpectedAttribute)
                            {
                                writer.WriteAttributeString(reader.Name, this.attributes[key]);
                                isExpectedAttribute = false;
                            }
                            else
                            {
                                writer.WriteAttributeString(reader.Name, reader.Value);
                            }
                            if (new string[] { "DeveloperToken", "Email", "Password" }.Contains(reader.Value))
                            {
                                key = reader.Value;
                                isExpectedAttribute = true;
                            }
                            
                        }

                        if (isSelfEnd)
                        {
                            writer.WriteEndElement();
                        }

                        break;
                    case XmlNodeType.EndElement:
                        writer.WriteEndElement();
                        break;
                    default:
                        break;
                }
            }
            writer.WriteEndDocument();

            reader.Close();
            writer.Close();
        }
    }
}
