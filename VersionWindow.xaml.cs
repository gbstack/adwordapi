﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AdwordsScraperGUI
{
    /// <summary>
    /// Interaction logic for VersionWindow.xaml
    /// </summary>
    public partial class VersionWindow : Window
    {
        public VersionWindow()
        {
            InitializeComponent();
        }

        public VersionWindow(string credits, string version)
        {
            InitializeComponent();
            this.creditsLabel.Content = credits;
            this.versionLabel.Content = "version:" + version;
            this.Show();
            this.creditsLabel.MouseEnter += delegate(Object sender, MouseEventArgs args)
            {
                this.Cursor = Cursors.Hand;
            };
            this.creditsLabel.MouseLeave += delegate(Object sender, MouseEventArgs args)
            {
                this.Cursor = Cursors.Arrow;
            };
            this.creditsLabel.MouseLeftButtonUp += delegate(Object sender, MouseButtonEventArgs args)
            {
                System.Diagnostics.Process.Start("IExplore.exe", Properties.Resources.site_url);
            };
        }
    }
}
